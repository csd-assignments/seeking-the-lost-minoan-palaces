package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import model.board.Board;
import model.card.*;
import model.finding.*;
import model.palace.Palace;
import model.player.Player;
import model.position.*;
import view.View;

/**
 * Controller is the master of the game and controls all of the operations executed
 *
 * @version 1.0
 * @author marianna vel
 */
public class Controller {

    private Board board;
    private View view;
    private Player player1, player2;
    private static boolean active; // for the listeners

    /**
     * <b>Constructor</b>: Constructs a new Controller and sets the game ready to start. So, is responsible for creating a new game and initializing it.
     * <b>Postcondition</b>: constructs a new Controller with new 2 players and initializes the game table, activates the listeners
     */
    public Controller() {
        player1 = new Player("Παίκτης1", 1);
        player2 = new Player("Παίκτης2", 2);
        board = new Board();
        board.initCards();
        board.initFindings();
        board.initPaths();
        player1.initPawns();
        player2.initPawns();
        view = new View();
        view.initComponents();
        view.initCards(board.getGameCards());
        for (int i = 0; i < 8; i++) {
            player1.setCard(board.getGameCards().remove(0), i);
        }
        for (int i = 0; i < 8; i++) {
            player2.setCard(board.getGameCards().remove(0), i);
        }
        Random rand = new Random();
        int n = rand.nextInt(2) + 1;
        if (n == 1) {
            board.setTurn(player1);
        } else if (n == 2) {
            board.setTurn(player2);
        }
        view.updateInfobox("<html>Available Cards: 84" + "<br>Check Points: 0" + "<br>Turn: " + board.getTurn().getName());
        view.setVisible(true);
        setListeners();
    }

    /**
     * <b>Transformer(mutative)</b>: activates the listeners
     * <b>Postcondition</b>: listeners are activated
     */
    private void setListeners() {
        active = true;
        for (int i = 0; i < view.getPlayerCards(player1).length; i++) {
            view.getPlayerCards(player1)[i].addMouseListener(new CardListener());
            view.getPlayerCards(player2)[i].addMouseListener(new CardListener());
        }
        view.getCardsDeck().addActionListener(new DeckListener());

        view.getFrescoBut1().addActionListener(new FrescoListener());
        view.getFrescoBut2().addActionListener(new FrescoListener());

        view.getNewGameItem().addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                Object[] options = {"Ναι", "Όχι"};
                int n = JOptionPane.showOptionDialog(view.getNewGameItem(),
                        "Θέλεις να ξεκινήσεις νέο παιχνίδι?", "Νέο Παιχνίδι",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[1]);

                if (n == JOptionPane.YES_OPTION) {
                    view.setVisible(false);
                    new Controller();
                }
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });
        view.getExitItem().addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                Object[] options = {"Ναι", "Όχι"};
                int n = JOptionPane.showOptionDialog(view.getNewGameItem(),
                        "Θέλεις να βγεις από το παιχνίδι?", "Έξοδος",
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[1]);

                if (n == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });
    }

    /**
     * <b>Transformer:</b> gets a card from the deck to add it at player's collection
     * <b>Postcondition:</b> new card from deck is returned
     *
     * @return top card
     */
    public Card GetCardFromDeck() {
        ArrayList<Card> deck = new ArrayList<Card>();
        deck = board.getGameCards();
        Card top = deck.get(deck.size() - 1);
        deck.remove(deck.size() - 1);
        board.setGameCards(deck);
        return top;
    }

    /**
     * <b>Transformer(mutative)</b>: Removes the card and replaces it with a new one from the deck
     * <b>Postcondition</b>: the card has been removed from the player's collection and replaced by another one
     *
     * @param cardIndex the position of card
     * @param player the player
     */
    public void removeAndReplaceCard(int cardIndex, Player player) {

        if (board.DeckIsEmpty()) {
            board.setGameOver();
            active = false;
            view.showDialogMessage("Τέλος Παιχνιδιού! Διαθέσιμες κάρτες: 0!");
            view.showWinningMessage(getWinner().getName() + " Συγχαρητήρια!!! Νίκησες!!!");
        } else {
            player.removeCard(cardIndex);
            player.setCard(board.getGameCards().remove(0), cardIndex);
            Card c = player.getCard(cardIndex);
            view.updateCard(c, cardIndex, player);
            updateTurn(player);
            view.updateInfobox("<html>Available Cards: " + board.getGameCards()
                    + "<br>Check Points: " + board.getCheckPoints()
                    + "<br>Turn: " + board.getTurn().getName());
            if (board.DeckIsEmpty()) {
                active = false;
                board.setGameOver();
                view.showDialogMessage("Τέλος Παιχνιδιού! Διαθέσιμες κάρτες: 0!");
                view.showWinningMessage(getWinner().getName() + " Συγχαρητήρια!!! Νίκησες!!!");
            } else {
                view.updateInfobox("<html>Available Cards: " + board.getGameCards().size()
                        + "<br>Check Points: " + board.getCheckPoints()
                        + "<br>Turn: " + board.getTurn().getName());
            }
        }
    }

    /**
     * <b>Transformer(mutative)</b>: Is used when a player has no pawn set at a path (instead movePawn)
     *
     * @param player the player
     * @param card the card that was played
     */
    public void startPawn(Player player, Card card) {
        int n, pawnIndex;

        //an o theseas den exei parei thesi se kapoio monopati
        if (player.getPawns()[0].getPosition() == -1) {
            int count = 0;
            for (int i = 1; i < 4; i++) {
                if (player.getPawns()[i].getPosition() == -1) {
                    count++;
                }
            }
            if (count == 0) {
                board.TheseusOption(player, card);
                view.updatePawn(player, 0, card.getPalace(), 0);
                view.updateAvailablePawns(player);
                return;
            }
            do {
                n = view.choosePawn();
                if (n == JOptionPane.YES_OPTION) {

                    board.TheseusOption(player, card);
                    view.updatePawn(player, 0, card.getPalace(), 0);
                    view.updateAvailablePawns(player);

                } else if (n == JOptionPane.NO_OPTION) {

                    pawnIndex = board.ArcheologistOption(player, card);
                    view.updatePawn(player, pawnIndex, card.getPalace(), 0);
                    view.updateAvailablePawns(player);

                } else {
                    view.showDialogMessage("Πρέπει να επιλέξεις πιόνι για να συνεχίσεις!");
                }
            } while (JOptionPane.YES_OPTION != n && JOptionPane.NO_OPTION != n);

        } else { //an o theseas exei paixtei

            pawnIndex = board.ArcheologistOption(player, card);
            view.updatePawn(player, pawnIndex, card.getPalace(), 0);
            view.updateAvailablePawns(player);
        }
    }

    /**
     * <b>Transformer(mutative)</b>: Checks if card in this position can be played above another
     * <b>Postcondition</b>: Checks if card in this position can be played above another
     *
     * @param cardIndex
     * @param player the player
     */
    public void checkIfItCanBePlayed(int cardIndex, Player player) {

        Card cardToPlay = player.getCard(cardIndex);
        NumberCard lastCardPlayed = (NumberCard) player.getLastCardPlayed(cardToPlay.getPalace());
        Palace palace = cardToPlay.getPalace();

        int index;
        /* otan exei paixtei karta se afto to monopati */
        if (lastCardPlayed != null) {
            if (cardToPlay instanceof NumberCard) {
                if (cardToPlay.matchCard(lastCardPlayed)) {

                    if (player.getPawn(palace).hasFinished()) {
                        return;
                    }
                    player.playCard((NumberCard) cardToPlay);
                    view.setLastCard(player, cardToPlay);
                    view.removePlayerCard(player, cardIndex);

                    index = player.findPawnIndex(player.getPawn(palace));
                    player.movePawn(board.getPawn(player, palace), cardToPlay);

                    (player.getPawn(palace)).setScore(board.getPosition(palace, player.getPawn(palace).getPosition()).getPoints());
                    player.setPositionScore();
                    view.updateScore(player, player.getTotalScore());

                    checkForFinding(board.getPosition(palace, player.getPawn(palace).getPosition()), player);

                    if (player.getPawn(palace).getPosition() == 6) {
                        board.setCheckPoint();
                    }
                    view.updatePawn(player, index, palace, player.getPawn(palace).getPosition());

                    if (player.getPawn(palace).getPosition() == 8) {
                        view.setPawnFinished(palace);
                    }

                } else {
                    view.showDialogMessage("Η αξία της κάρτας πρέπει να είναι μεγαλύτερη από την προηγούμενη που παίχτηκε!");
                }
            } else if (cardToPlay instanceof Ariadne) {

                if (player.getPawn(palace).hasFinished()) {
                    return;
                }
                player.removeCard(cardIndex);
                view.setLastCard(player, cardToPlay);
                view.removePlayerCard(player, cardIndex);

                index = player.findPawnIndex(player.getPawn(palace));
                player.movePawn(board.getPawn(player, palace), cardToPlay);

                (player.getPawn(palace)).setScore(board.getPosition(palace, player.getPawn(palace).getPosition()).getPoints());
                player.setPositionScore();
                view.updateScore(player, player.getTotalScore());

                checkForFinding(board.getPosition(palace, player.getPawn(palace).getPosition()), player);

                if (player.getPawn(palace).getPosition() >= 6 && player.getPawn(palace).getPosition() != 8) {
                    board.setCheckPoint();
                }
                view.updatePawn(player, index, palace, player.getPawn(palace).getPosition());

                if (player.getPawn(palace).getPosition() == 8) {
                    view.setPawnFinished(palace);
                }

            } else if (cardToPlay instanceof Minotaur) {

                player.removeCard(cardIndex);
                view.setLastCard(player, cardToPlay);
                view.removePlayerCard(player, cardIndex);

                if (player.getID() == 1) {
                    if (player2.getPawn(palace) != null) {
                        index = player2.findPawnIndex(player2.getPawn(palace));
                        player2.movePawn(board.getPawn(player2, palace), cardToPlay);

                        (player2.getPawn(palace)).setScore(board.getPosition(palace, player2.getPawn(palace).getPosition()).getPoints());
                        player2.setPositionScore();
                        view.updateScore(player2, player.getTotalScore());

                        checkForFinding(board.getPosition(palace, player2.getPawn(palace).getPosition()), player2);
                        view.updatePawn(player2, index, palace, player2.getPawn(palace).getPosition());
                    }
                } else if (player.getID() == 2) {
                    if (player1.getPawn(palace) != null) {
                        index = player1.findPawnIndex(player1.getPawn(palace));
                        player1.movePawn(board.getPawn(player1, palace), cardToPlay);

                        (player1.getPawn(palace)).setScore(board.getPosition(palace, player1.getPawn(palace).getPosition()).getPoints());
                        player1.setPositionScore();
                        view.updateScore(player1, player.getTotalScore());

                        checkForFinding(board.getPosition(palace, player1.getPawn(palace).getPosition()), player1);
                        view.updatePawn(player1, index, palace, player1.getPawn(palace).getPosition());
                    }
                }
            }
        } else {
            /* otan den exei paixtei karta se afto to monopati */
            if (cardToPlay instanceof NumberCard) {

                player.playCard((NumberCard) cardToPlay);
                view.setLastCard(player, cardToPlay);
                view.removePlayerCard(player, cardIndex);
                startPawn(player, cardToPlay);

                (player.getPawn(palace)).setScore(board.getPosition(palace, player.getPawn(palace).getPosition()).getPoints());
                player.setPositionScore();
                view.updateScore(player, player.getTotalScore());

            } else if (cardToPlay instanceof Ariadne) {
                view.showDialogMessage("Αυτή η κάρτα δε μπορεί να παιχτεί στην αρχή");
            } else if (cardToPlay instanceof Minotaur) {

                player.removeCard(cardIndex);
                view.setLastCard(player, cardToPlay);
                view.removePlayerCard(player, cardIndex);

                if (player.getID() == 1) {
                    if (player2.getLastCardPlayed(palace) != null) {
                        index = player2.findPawnIndex(player2.getPawn(palace));
                        player2.movePawn(board.getPawn(player2, palace), cardToPlay);

                        (player2.getPawn(palace)).setScore(board.getPosition(palace, player2.getPawn(palace).getPosition()).getPoints());
                        player2.setPositionScore();
                        view.updateScore(player2, player.getTotalScore());
                        view.updatePawn(player2, index, palace, player2.getPawn(palace).getPosition());
                    }
                } else if (player.getID() == 2) {
                    if (player1.getLastCardPlayed(palace) != null) {
                        index = player1.findPawnIndex(player1.getPawn(palace));
                        player1.movePawn(board.getPawn(player1, palace), cardToPlay);

                        (player1.getPawn(palace)).setScore(board.getPosition(palace, player1.getPawn(palace).getPosition()).getPoints());
                        player1.setPositionScore();
                        view.updateScore(player1, player.getTotalScore());
                        view.updatePawn(player1, index, palace, player1.getPawn(palace).getPosition());
                    }
                }
            }
        }
        if (player.getID() == 1) {
            player1 = player;
        } else if (player.getID() == 2) {
            player2 = player;
        }
        view.updateInfobox("<html>Available Cards: " + board.getGameCards().size()
                + "<br>Check Points: " + board.getCheckPoints()
                + "<br>Turn: " + board.getTurn().getName());
    }

    /**
     * <b>Transformer</b>: If the position has finding sets it at player's findings <br/>
     * <b>Postcondition:</b> If the position has finding sets it at player's findings, finding has been removed
     *
     * @param position the current position of player's pawn
     * @param player the player
     */
    public void checkForFinding(Position position, Player player) {
        if (position instanceof FindingPosition) {
            if (position.hasFinding()) {
                Finding finding = ((FindingPosition) position).getFinding();

                if (finding instanceof RareFinding) {
                    player.setFinding(finding);
                    view.updateRareFindings(player, position.getPalace());
                    position.removeFinding();
                }
                if (finding instanceof Fresco) {
                    for (int i = 0; i < player.getFrescos().size(); i++) {
                        if (player.getFrescos().get(i) == finding) {
                            return;
                        }
                    }
                    player.setFinding(finding);
                    view.updateFresco(player, (Fresco) finding);
                }
                if (finding instanceof SnakeGoddess) {
                    player.setFinding(finding);
                    view.updateStatues(player, player.numberOfStatues());
                    position.removeFinding();
                }
            }
        }
    }

    /**
     * <b>Transformer(mutative)</b>: Updates the players turn
     * <b>Postcondition</b>: the players turn has been updated
     *
     * @param player the player that played last
     */
    public void updateTurn(Player player) {
        if (player.getID() == 1) {
            board.setTurn(player2);
        } else {
            board.setTurn(player1);
        }
    }

    public boolean checkIfGameFinished() {
        if (board.getCheckPoints() == 4) {
            board.setGameOver();
            active = false;            
            view.showDialogMessage("Τέλος παιχνιδιού! 4 πιόνια πέρασαν από το 7ο βημα!");
            view.showWinningMessage(getWinner().getName() + " Συγχαρητήρια!!! Νίκησες!!!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Accessor(selector)</b>: Returns the winner
     * <b>Postcondition</b>: the winner is returned
     *
     * @return th winner
     */
    public Player getWinner() {
        if (player1.getTotalScore() > player2.getTotalScore()) {
            return player1;
        } else {
            return player2;
        }
    }
//---------------------------------------------------------------------------------------------------------

    /* a class which is used for doing some action after a card button has been pushed */
    class CardListener implements MouseListener {

        /**
         * <b>Transformer(mutative)</b>: doing some action after a player's card has been clicked<br/>
         * <p>
         * <b>Postcondition:</b> doing some action after a player's card has been clicked</p>
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            if (active) {
                JButton but = ((JButton) e.getSource());
                if (board.GameOver() == true) {
                    return;
                }
                int cardIndex = -1;
                Player player = new Player();
                for (int i = 0; i < 8; i++) {
                    if (but == view.getPlayerCards(player1)[i]) {
                        if (board.getTurn() == player2) {
                            view.showDialogMessage("Ο Παίκτης2 δεν ολοκλήρωσε τη σειρά του");
                            return;
                        }
                        player = player1;
                        cardIndex = i;
                        break;
                    } else if (but == view.getPlayerCards(player2)[i]) {
                        if (board.getTurn() == player1) {
                            view.showDialogMessage("Ο Παίκτης1 δεν ολοκλήρωσε τη σειρά του");
                            return;
                        }
                        player = player2;
                        cardIndex = i;
                        break;
                    }
                }
                if (SwingUtilities.isLeftMouseButton(e)) {
                    checkIfItCanBePlayed(cardIndex, player);
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    removeAndReplaceCard(cardIndex, player);
                }

                if (checkIfGameFinished()) {
                    return;
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

    }

    /* a class which is used for doing some action after the deck button has been pushed */
    class DeckListener implements ActionListener {

        /**
         * <b>Transformer(mutative)</b>: doing some action after the Deck button has been pushed<br/>
         * <p>
         * <b>Postcondition:</b> doing some action after the Deck button has been pushed</p>
         */
        @Override
        public void actionPerformed(ActionEvent e) {

            if (active) {
                JButton but = ((JButton) e.getSource());
                Card cardFromDeck = GetCardFromDeck();

                int cardIndex = -1;
                for (int i = 0; i < 8; i++) {
                    if (player1.getCard(i) == null) {
                        cardIndex = i;
                        break;
                    }
                    if (player2.getCard(i) == null) {
                        cardIndex = i;
                        break;
                    }
                }
                if (cardIndex < 0) {
                    view.showDialogMessage("Δε μπορείς να ξανατραβήξεις κάρτα!");
                    return;
                }
                Player player = board.getTurn();
                player.setCard(cardFromDeck, cardIndex);
                Card c = player.getCard(cardIndex);
                view.updateCard(c, cardIndex, player);
                updateTurn(player);

                if (board.DeckIsEmpty()) {
                    board.setGameOver();
                    view.showDialogMessage("Τέλος Παιχνιδιού! Διαθέσιμες κάρτες: 0!");
                    view.showWinningMessage(getWinner().getName() + " Συγχαρητήρια!!! Νίκησες!!!");
                    return;
                } else {
                    view.updateInfobox("<html>Available Cards: " + board.getGameCards().size()
                            + "<br>Check Points: " + board.getCheckPoints()
                            + "<br>Turn: " + board.getTurn().getName());
                }
            }
        }
    }

    /* a class which is used for doing some action after a fresco button has been pushed */
    class FrescoListener implements ActionListener {

        /**
         * <b>Transformer(mutative)</b>: doing some action after the Fresco button has been pushed<br/>
         * <p>
         * <b>Postcondition:</b> doing some action after the Fresco button has been pushed</p>
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton but = ((JButton) e.getSource());

            if (but == view.getFrescoBut1()) {
                view.OpenFrescoWindow(player1);
            } else if (but == view.getFrescoBut2()) {
                view.OpenFrescoWindow(player2);
            }
        }
    }
}
