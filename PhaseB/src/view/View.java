package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.*;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.*;
import model.card.Card;
import model.finding.*;
import model.palace.Palace;
import static model.palace.Palace.*;
import model.player.Player;

/**
 * The View represents whatever is shown on the screen during the execution. All the components are initialized here and the image is updated as long as the game evolutes.
 *
 * @version 1.0
 * @author marianna vel
 */
public class View extends JFrame {

    private ClassLoader cldr;
    //Menu
    private JMenuBar mb;
    private JMenu item1, item2;

    //Table Components
    private JLayeredPaneExtension tablo;
    private JButton CardStack = new JButton();
    private JLabel[] KnossosPath = new JLabel[9];
    private JLabel[] FaistosPath = new JLabel[9];
    private JLabel[] MaliaPath = new JLabel[9];
    private JLabel[] ZakrosPath = new JLabel[9];
    private JTextField[] Points = new JTextField[9];
    private JTextField CheckPoint;
    private JLabel infobox = new JLabel();
    private JLabel[] Pawn1 = new JLabel[4];
    private JLabel[] Pawn2 = new JLabel[4];

    //1st player's field Components
    private JLayeredPane player1;
    private JButton[] PlayerCards1 = new JButton[8];
    private JLabel[] LastChoice1 = new JLabel[4];
    private JLabel[] FindingImage1 = new JLabel[4];
    private JLabel Goddess1;
    private JButton FrescoButton1;
    private JLabel AvailablePawns1;
    private JLabel Score1;
    private JLabel Statues1;
    //for the fresco button:
    private JFrame frame1;
    private JPanel imgPanel1;
    private Image fresco1, fresco2, fresco3;
    private Image fresco4, fresco5, fresco6;
    private JLabel fresco1Label1;
    private JLabel fresco2Label1;
    private JLabel fresco3Label1;
    private JLabel fresco4Label1;
    private JLabel fresco5Label1;
    private JLabel fresco6Label1;

    //2nd player's field Components
    private JLayeredPane player2;
    private JButton[] PlayerCards2 = new JButton[8];
    private JLabel[] LastChoice2 = new JLabel[4];
    private JLabel[] FindingImage2 = new JLabel[4];
    private JLabel Goddess2;
    private JButton FrescoButton2;
    private JLabel AvailablePawns2;
    private JLabel Score2;
    private JLabel Statues2;
    //for the fresco button:
    private JFrame frame2;
    private JPanel imgPanel2;
    private JLabel fresco1Label2;
    private JLabel fresco2Label2;
    private JLabel fresco3Label2;
    private JLabel fresco4Label2;
    private JLabel fresco5Label2;
    private JLabel fresco6Label2;

    /**
     * <b>Constructor</b>: Creates a new Window and initializes some buttons and panels <br/>
     * <b>Postconditions</b>: Creates a new Window and initializes some buttons and panels starting a new game.
     */
    public View() {
        cldr = this.getClass().getClassLoader();
        this.setResizable(true);
        this.setTitle("Αναζητώντας τα Χαμένα Μινωικά Ανάκτορα");
        mb = new JMenuBar();
        item1 = new JMenu("New Game");
        item2 = new JMenu("Exit");
        item1.setFont(item1.getFont().deriveFont(14.0f));
        item2.setFont(item2.getFont().deriveFont(14.0f));
        mb.add(item1);
        mb.add(item2);
        this.setJMenuBar(mb);
        this.setPreferredSize(new Dimension(1340, 965));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * <b>Transformer(mutative)</b>:initializes some buttons and labels <br/>
     * <p>
     * <b>Postcondition:</b> initializes some buttons and labels </p>
     */
    public void initComponents() {

        /**
         * ** player1 ***
         */
        player1 = new JLayeredPane();
        player1.setBounds(0, 0, 1320, 180);
        player1.setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.RED));
        player1.setBackground(Color.white);
        player1.setOpaque(true);
        this.add(player1);

        /**
         * ** basic panel ***
         */
        URL imageURL = cldr.getResource("images/background.jpg"); //image
        Image image = new ImageIcon(imageURL).getImage();
        tablo = new JLayeredPaneExtension(image);
        tablo.setBounds(5, 170, 1330, 930);
        this.add(tablo);

        /**
         * ** player2 ***
         */
        player2 = new JLayeredPane();
        player2.setBounds(0, 712, 1320, 180);
        player2.setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.GREEN));
        player2.setBackground(Color.white);
        player2.setOpaque(true);
        this.add(player2);

        GroupLayout group = new GroupLayout(getContentPane());
        getContentPane().setLayout(group);

        /**
         * *** deck of cards ****
         */
        URL DeckURL = cldr.getResource("images/cards/backCard.jpg");
        Image backCard = new ImageIcon(DeckURL).getImage();
        backCard = backCard.getScaledInstance(90, 130, java.awt.Image.SCALE_SMOOTH);
        CardStack.setBounds(50, 280, 90, 130);
        CardStack.setIcon(new ImageIcon(backCard));
        tablo.add(CardStack);

        /**
         * *** information box ****
         */
        infobox.setBounds(20, 430, 180, 80);
        infobox.setBackground(Color.WHITE);
        infobox.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.BLACK));
        infobox.setOpaque(true);
        //infobox.setText("");
        infobox.setFont(infobox.getFont().deriveFont(16.0f));
        tablo.add(infobox, 0);

        /**
         * *** paths ****
         */
        URL knossosURL = cldr.getResource("images/paths/knossos.jpg");
        Image knossos = new ImageIcon(knossosURL).getImage();
        knossos = knossos.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL knossosURL2 = cldr.getResource("images/paths/knossos2.jpg");
        Image knossos2 = new ImageIcon(knossosURL2).getImage();
        knossos2 = knossos2.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL knossosURLPalace = cldr.getResource("images/paths/knossosPalace.jpg");
        Image knossosPalace = new ImageIcon(knossosURLPalace).getImage();
        knossosPalace = knossosPalace.getScaledInstance(120, 80, java.awt.Image.SCALE_SMOOTH);

        URL maliaURL = cldr.getResource("images/paths/malia.jpg");
        Image malia = new ImageIcon(maliaURL).getImage();
        malia = malia.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL maliaURL2 = cldr.getResource("images/paths/malia2.jpg");
        Image malia2 = new ImageIcon(maliaURL2).getImage();
        malia2 = malia2.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL maliaURLPalace = cldr.getResource("images/paths/maliaPalace.jpg");
        Image maliaPalace = new ImageIcon(maliaURLPalace).getImage();
        maliaPalace = maliaPalace.getScaledInstance(120, 80, java.awt.Image.SCALE_SMOOTH);

        URL phaistosURL = cldr.getResource("images/paths/phaistos.jpg");
        Image phaistos = new ImageIcon(phaistosURL).getImage();
        phaistos = phaistos.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL phaistosURL2 = cldr.getResource("images/paths/phaistos2.jpg");
        Image phaistos2 = new ImageIcon(phaistosURL2).getImage();
        phaistos2 = phaistos2.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL phaistosURLPalace = cldr.getResource("images/paths/phaistosPalace.jpg");
        Image phaistosPalace = new ImageIcon(phaistosURLPalace).getImage();
        phaistosPalace = phaistosPalace.getScaledInstance(120, 80, java.awt.Image.SCALE_SMOOTH);

        URL zakrosURL = cldr.getResource("images/paths/zakros.jpg");
        Image zakros = new ImageIcon(zakrosURL).getImage();
        zakros = zakros.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL zakrosURL2 = cldr.getResource("images/paths/zakros2.jpg");
        Image zakros2 = new ImageIcon(zakrosURL2).getImage();
        zakros2 = zakros2.getScaledInstance(85, 75, java.awt.Image.SCALE_SMOOTH);
        URL zakrosURLPalace = cldr.getResource("images/paths/zakrosPalace.jpg");
        Image zakrosPalace = new ImageIcon(zakrosURLPalace).getImage();
        zakrosPalace = zakrosPalace.getScaledInstance(120, 80, java.awt.Image.SCALE_SMOOTH);

        for (int i = 0; i < 9; i++) {
            KnossosPath[i] = new JLabel("" + (i + 1));
            KnossosPath[i].setBackground(Color.white);
            KnossosPath[i].setOpaque(true);
            if (i % 2 == 0) {
                if (i == 8) {
                    KnossosPath[i].setBounds(440 + i * 90, 100, 120, 80);
                    KnossosPath[i].setIcon(new ImageIcon(knossosPalace));
                } else {
                    KnossosPath[i].setBounds(440 + i * 90, 100, 85, 75);
                    KnossosPath[i].setIcon(new ImageIcon(knossos));
                }
            } else {
                KnossosPath[i].setBounds(440 + i * 90, 100, 85, 75);
                KnossosPath[i].setIcon(new ImageIcon(knossos2));
            }
            tablo.add(KnossosPath[i], 0);
        }

        for (int i = 0; i < 9; i++) {
            MaliaPath[i] = new JLabel("" + (i + 1));
            MaliaPath[i].setBackground(Color.white);
            MaliaPath[i].setOpaque(true);
            if (i % 2 == 0) {
                if (i == 8) {
                    MaliaPath[i].setBounds(440 + i * 90, 200, 120, 80);
                    MaliaPath[i].setIcon(new ImageIcon(maliaPalace));
                } else {
                    MaliaPath[i].setBounds(440 + i * 90, 200, 85, 75);
                    MaliaPath[i].setIcon(new ImageIcon(malia));
                }
            } else {
                MaliaPath[i].setBounds(440 + i * 90, 200, 85, 75);
                MaliaPath[i].setIcon(new ImageIcon(malia2));
            }
            tablo.add(MaliaPath[i], 0);
        }

        for (int i = 0; i < 9; i++) {
            FaistosPath[i] = new JLabel("" + (i + 1));
            FaistosPath[i].setBackground(Color.white);
            FaistosPath[i].setOpaque(true);
            if (i % 2 == 0) {
                if (i == 8) {
                    FaistosPath[i].setBounds(440 + i * 90, 300, 120, 80);
                    FaistosPath[i].setIcon(new ImageIcon(phaistosPalace));
                } else {
                    FaistosPath[i].setBounds(440 + i * 90, 300, 85, 75);
                    FaistosPath[i].setIcon(new ImageIcon(phaistos));
                }
            } else {
                FaistosPath[i].setBounds(440 + i * 90, 300, 85, 75);
                FaistosPath[i].setIcon(new ImageIcon(phaistos2));
            }
            tablo.add(FaistosPath[i], 0);
        }

        for (int i = 0; i < 9; i++) {
            ZakrosPath[i] = new JLabel("" + (i + 1));
            ZakrosPath[i].setBackground(Color.white);
            ZakrosPath[i].setOpaque(true);
            if (i % 2 == 0) {
                if (i == 8) {
                    ZakrosPath[i].setBounds(440 + i * 90, 400, 120, 80);
                    ZakrosPath[i].setIcon(new ImageIcon(zakrosPalace));
                } else {
                    ZakrosPath[i].setBounds(440 + i * 90, 400, 85, 75);
                    ZakrosPath[i].setIcon(new ImageIcon(zakros));
                }
            } else {
                ZakrosPath[i].setBounds(440 + i * 90, 400, 85, 75);
                ZakrosPath[i].setIcon(new ImageIcon(zakros2));
            }
            tablo.add(ZakrosPath[i], 0);
        }

        /**
         * ** points ***
         */
        CheckPoint = new JTextField("Check Point!");
        //CheckPoint.setFont(CheckPoint.getFont().deriveFont(14.0f));

        Font myFont = new Font("Courier", Font.BOLD, 14);
        CheckPoint.setFont(myFont);

        CheckPoint.setBounds(982, 40, 90, 90);
        tablo.add(CheckPoint, JLayeredPane.DEFAULT_LAYER);
        CheckPoint.setBorder(null);
        CheckPoint.setOpaque(false);
        CheckPoint.setEditable(false);

        Points[0] = new JTextField("-20 points");
        Points[1] = new JTextField("-15 points");
        Points[2] = new JTextField("-10 points");
        Points[3] = new JTextField("5 points");
        Points[4] = new JTextField("10 points");
        Points[5] = new JTextField("15 points");
        Points[6] = new JTextField("30 points");
        Points[7] = new JTextField("35 points");
        Points[8] = new JTextField("50 points");

        for (int i = 0; i < 9; i++) {
            Points[i].setBounds(445 + i * 90, 20, 85, 85);
            Points[i].setFont(myFont);
            Points[i].setBorder(null);
            Points[i].setOpaque(false);
            Points[i].setEditable(false);
            tablo.add(Points[i]);
        }

        /**
         * ** last cards played ***
         */
        for (int i = 0; i < 4; i++) {
            LastChoice1[i] = new JLabel();
            LastChoice1[i].setBounds(750 + i * 90, 10, 80, 115);
            LastChoice1[i].setBackground(Color.WHITE);
            LastChoice1[i].setOpaque(true);
            LastChoice1[i].setFont(LastChoice1[i].getFont().deriveFont(16.0f));
            player1.add(LastChoice1[i]);

            LastChoice2[i] = new JLabel();
            LastChoice2[i].setBounds(750 + i * 90, 10, 80, 115);
            LastChoice2[i].setBackground(Color.WHITE);
            LastChoice2[i].setOpaque(true);
            LastChoice2[i].setFont(LastChoice2[i].getFont().deriveFont(16.0f));
            player2.add(LastChoice2[i]);
        }
        LastChoice1[0].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
        LastChoice1[0].setText("Κνωσσός");
        LastChoice2[0].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
        LastChoice2[0].setText("Κνωσσός");
        LastChoice1[1].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.YELLOW));
        LastChoice1[1].setText("Μάλια");
        LastChoice2[1].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.YELLOW));
        LastChoice2[1].setText("Μάλια");
        LastChoice1[2].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));
        LastChoice1[2].setText("Φαιστός");
        LastChoice2[2].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));
        LastChoice2[2].setText("Φαιστός");
        LastChoice1[3].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.BLUE));
        LastChoice1[3].setText("Ζάκρος");
        LastChoice2[3].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.BLUE));
        LastChoice2[3].setText("Ζάκρος");

        /**
         * ** rare findings ***
         */
        for (int i = 0; i < 4; i++) {
            FindingImage1[i] = new JLabel();
            FindingImage1[i].setBounds(765 + i * 90, 125, 50, 50);
            player1.add(FindingImage1[i]);
            FindingImage2[i] = new JLabel();
            FindingImage2[i].setBounds(765 + i * 90, 125, 50, 50);
            player2.add(FindingImage2[i]);
        }
        URL ringURL = cldr.getResource("images/findings/ring.jpg");
        Image ring = new ImageIcon(ringURL).getImage();
        ring = ring.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
        FindingImage1[0].setIcon(new ImageIcon(createGrayEffect(ring)));
        FindingImage2[0].setIcon(new ImageIcon(createGrayEffect(ring)));

        URL kosmimaURL = cldr.getResource("images/findings/kosmima.jpg");
        Image kosmima = new ImageIcon(kosmimaURL).getImage();
        kosmima = kosmima.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
        FindingImage1[1].setIcon(new ImageIcon(createGrayEffect(kosmima)));
        FindingImage2[1].setIcon(new ImageIcon(createGrayEffect(kosmima)));

        URL diskosURL = cldr.getResource("images/findings/diskos.jpg");
        Image diskos = new ImageIcon(diskosURL).getImage();
        diskos = diskos.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
        FindingImage1[2].setIcon(new ImageIcon(createGrayEffect(diskos)));
        FindingImage2[2].setIcon(new ImageIcon(createGrayEffect(diskos)));

        URL rutoURL = cldr.getResource("images/findings/ruto.jpg");
        Image ruto = new ImageIcon(rutoURL).getImage();
        ruto = ruto.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
        FindingImage1[3].setIcon(new ImageIcon(createGrayEffect(ruto)));
        FindingImage2[3].setIcon(new ImageIcon(createGrayEffect(ruto)));

        /**
         * ** pawns ***
         */
        URL theseusURL = cldr.getResource("images/pionia/theseus.jpg");
        Image theseus = new ImageIcon(theseusURL).getImage();
        theseus = theseus.getScaledInstance(40, 60, java.awt.Image.SCALE_SMOOTH);
        Pawn1[0] = new JLabel();
        Pawn1[0].setIcon(new ImageIcon(theseus));
        Pawn1[0].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
        Pawn2[0] = new JLabel();
        Pawn2[0].setIcon(new ImageIcon(theseus));
        Pawn2[0].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GREEN));

        URL archURL = cldr.getResource("images/pionia/arch.jpg");
        Image arch = new ImageIcon(archURL).getImage();
        arch = arch.getScaledInstance(40, 60, java.awt.Image.SCALE_SMOOTH);
        for (int i = 1; i < 4; i++) {
            Pawn1[i] = new JLabel();
            Pawn1[i].setIcon(new ImageIcon(arch));
            Pawn1[i].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
            Pawn2[i] = new JLabel();
            Pawn2[i].setIcon(new ImageIcon(arch));
            Pawn2[i].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GREEN));
        }

        AvailablePawns1 = new JLabel();
        AvailablePawns1.setBounds(10, 0, 400, 320);
        AvailablePawns1.setBorder(null);
        AvailablePawns1.setOpaque(false);
        AvailablePawns1.setText("Παίκτης1" + " - Διαθέσιμα Πιόνια: 3 Αρχαιολόγοι και 1 Θησέας");
        AvailablePawns1.setFont(AvailablePawns1.getFont().deriveFont(14.0f));
        player1.add(AvailablePawns1);

        AvailablePawns2 = new JLabel();
        AvailablePawns2.setBounds(10, 0, 400, 320);
        AvailablePawns2.setBorder(null);
        AvailablePawns2.setOpaque(false);
        AvailablePawns2.setText("Παίκτης2" + " - Διαθέσιμα Πιόνια: 3 Αρχαιολόγοι και 1 Θησέας");
        AvailablePawns2.setFont(AvailablePawns2.getFont().deriveFont(14.0f));
        player2.add(AvailablePawns2);

        /**
         * ** score ***
         */
        Score1 = new JLabel();
        Score1.setBounds(1120, 0, 170, 80);
        Score1.setBorder(null);
        Score1.setOpaque(false);
        Score1.setText("Το Σκορ Μου: " + 0 + " Πόντοι");
        Score1.setFont(Score1.getFont().deriveFont(14.0f));
        player1.add(Score1);

        Score2 = new JLabel();
        Score2.setBounds(1120, 0, 170, 80);
        Score2.setBorder(null);
        Score2.setOpaque(false);
        Score2.setText("Το Σκορ Μου: " + 0 + " Πόντοι");
        Score2.setFont(Score2.getFont().deriveFont(14.0f));
        player2.add(Score2);

        /**
         * ** fresco ***
         */
        FrescoButton1 = new JButton();
        FrescoButton1.setBounds(1120, 70, 180, 40);
        FrescoButton1.setText("Οι Τοιχογραφίες μου");
        FrescoButton1.setFont(FrescoButton1.getFont().deriveFont(14.0f));
        player1.add(FrescoButton1);

        FrescoButton2 = new JButton();
        FrescoButton2.setBounds(1120, 70, 180, 40);
        FrescoButton2.setText("Οι Τοιχογραφίες μου");
        FrescoButton2.setFont(FrescoButton1.getFont().deriveFont(14.0f));
        player2.add(FrescoButton2);

        URL fresco1URL = cldr.getResource("images/findings/fresco1_20.jpg");
        fresco1 = new ImageIcon(fresco1URL).getImage();
        fresco1 = fresco1.getScaledInstance(190, 130, java.awt.Image.SCALE_SMOOTH);

        URL fresco2URL = cldr.getResource("images/findings/fresco2_20.jpg");
        fresco2 = new ImageIcon(fresco2URL).getImage();
        fresco2 = fresco2.getScaledInstance(190, 130, java.awt.Image.SCALE_SMOOTH);

        URL fresco3URL = cldr.getResource("images/findings/fresco3_15.jpg");
        fresco3 = new ImageIcon(fresco3URL).getImage();
        fresco3 = fresco3.getScaledInstance(190, 130, java.awt.Image.SCALE_SMOOTH);

        URL fresco4URL = cldr.getResource("images/findings/fresco4_20.jpg");
        fresco4 = new ImageIcon(fresco4URL).getImage();
        fresco4 = fresco4.getScaledInstance(180, 130, java.awt.Image.SCALE_SMOOTH);

        URL fresco5URL = cldr.getResource("images/findings/fresco5_15.jpg");
        fresco5 = new ImageIcon(fresco5URL).getImage();
        fresco5 = fresco5.getScaledInstance(180, 130, java.awt.Image.SCALE_SMOOTH);

        URL fresco6URL = cldr.getResource("images/findings/fresco6_15.jpg");
        fresco6 = new ImageIcon(fresco6URL).getImage();
        fresco6 = fresco6.getScaledInstance(180, 130, java.awt.Image.SCALE_SMOOTH);

        fresco1Label1 = new JLabel();
        fresco2Label1 = new JLabel();
        fresco3Label1 = new JLabel();
        fresco4Label1 = new JLabel();
        fresco5Label1 = new JLabel();
        fresco6Label1 = new JLabel();

        fresco1Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco1Label1.setVerticalAlignment(JLabel.CENTER);
        fresco2Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco2Label1.setVerticalAlignment(JLabel.CENTER);
        fresco3Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco3Label1.setVerticalAlignment(JLabel.CENTER);
        fresco4Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco4Label1.setVerticalAlignment(JLabel.CENTER);
        fresco5Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco5Label1.setVerticalAlignment(JLabel.CENTER);
        fresco6Label1.setHorizontalAlignment(JLabel.CENTER);
        fresco6Label1.setVerticalAlignment(JLabel.CENTER);

        fresco1Label1.setIcon(new ImageIcon(createGrayEffect(fresco1)));
        fresco2Label1.setIcon(new ImageIcon(createGrayEffect(fresco2)));
        fresco3Label1.setIcon(new ImageIcon(createGrayEffect(fresco3)));
        fresco4Label1.setIcon(new ImageIcon(createGrayEffect(fresco4)));
        fresco5Label1.setIcon(new ImageIcon(createGrayEffect(fresco5)));
        fresco6Label1.setIcon(new ImageIcon(createGrayEffect(fresco6)));
        
        //-------------------------------------------------------------
        
        fresco1Label2 = new JLabel();
        fresco2Label2 = new JLabel();
        fresco3Label2 = new JLabel();
        fresco4Label2 = new JLabel();
        fresco5Label2 = new JLabel();
        fresco6Label2 = new JLabel();

        fresco1Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco1Label2.setVerticalAlignment(JLabel.CENTER);
        fresco2Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco2Label2.setVerticalAlignment(JLabel.CENTER);
        fresco3Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco3Label2.setVerticalAlignment(JLabel.CENTER);
        fresco4Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco4Label2.setVerticalAlignment(JLabel.CENTER);
        fresco5Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco5Label2.setVerticalAlignment(JLabel.CENTER);
        fresco6Label2.setHorizontalAlignment(JLabel.CENTER);
        fresco6Label2.setVerticalAlignment(JLabel.CENTER);

        fresco1Label2.setIcon(new ImageIcon(createGrayEffect(fresco1)));
        fresco2Label2.setIcon(new ImageIcon(createGrayEffect(fresco2)));
        fresco3Label2.setIcon(new ImageIcon(createGrayEffect(fresco3)));
        fresco4Label2.setIcon(new ImageIcon(createGrayEffect(fresco4)));
        fresco5Label2.setIcon(new ImageIcon(createGrayEffect(fresco5)));
        fresco6Label2.setIcon(new ImageIcon(createGrayEffect(fresco6)));

        /**
         * ** statues ***
         */
        Statues1 = new JLabel();
        Statues1.setBounds(1120, 100, 150, 80);
        Statues1.setBorder(null);
        Statues1.setOpaque(false);
        Statues1.setText("Αγαλματάκια: " + 0);
        Statues1.setFont(Statues1.getFont().deriveFont(14.0f));
        player1.add(Statues1);

        Statues2 = new JLabel();
        Statues2.setBounds(1120, 100, 150, 80);
        Statues2.setBorder(null);
        Statues2.setOpaque(false);
        Statues2.setText("Αγαλματάκια: " + 0);
        Statues2.setFont(Statues2.getFont().deriveFont(14.0f));
        player2.add(Statues2);

        URL goddessURL = cldr.getResource("images/findings/snakes.jpg");
        Image goddess = new ImageIcon(goddessURL).getImage();
        goddess = goddess.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);

        Goddess1 = new JLabel();
        Goddess1.setBounds(1240, 125, 50, 50);
        Goddess1.setIcon(new ImageIcon(goddess));
        player1.add(Goddess1);
        Goddess2 = new JLabel();
        Goddess2.setBounds(1240, 125, 50, 50);
        Goddess2.setIcon(new ImageIcon(goddess));
        player2.add(Goddess2);

        pack();
    }

    public void initCards(ArrayList<Card> CardsDeck) {

        for (int i = 0; i < 8; i++) {
            PlayerCards1[i] = new JButton();
            PlayerCards1[i].setBounds(10 + i * 90, 10, 80, 115);
            URL imageURL1 = cldr.getResource(CardsDeck.get(i).getImage());
            Image image1 = new ImageIcon(imageURL1).getImage();
            image1 = image1.getScaledInstance(80, 115, java.awt.Image.SCALE_SMOOTH);
            PlayerCards1[i].setIcon(new ImageIcon(image1));
            player1.add(PlayerCards1[i]);

            PlayerCards2[i] = new JButton();
            PlayerCards2[i].setBounds(10 + i * 90, 10, 80, 115);
            URL imageURL2 = cldr.getResource(CardsDeck.get(i + 8).getImage());
            Image image2 = new ImageIcon(imageURL2).getImage();
            image2 = image2.getScaledInstance(80, 115, java.awt.Image.SCALE_SMOOTH);
            PlayerCards2[i].setIcon(new ImageIcon(image2));
            player2.add(PlayerCards2[i]);
        }
    }

    /**
     * Returns the image with gray effect
     *
     * @param wannabegray the image
     * @return image with gray effect
     */
    public Image createGrayEffect(Image wannabegray) {
        ImageFilter thefilter = new GrayFilter(true, 70);
        ImageProducer producer = new FilteredImageSource(wannabegray.getSource(), thefilter);
        return Toolkit.getDefaultToolkit().createImage(producer);
    }

    public void setLastCard(Player player, Card c, int index) {
        URL imageURL2 = cldr.getResource(c.getImage());
        Image image2 = new ImageIcon(imageURL2).getImage();
        image2 = image2.getScaledInstance(80, 115, java.awt.Image.SCALE_SMOOTH);
        if (player.getID() == 1) {
            LastChoice1[index].setIcon(new ImageIcon(image2));
        } else if (player.getID() == 2) {
            LastChoice2[index].setIcon(new ImageIcon(image2));
        }
    }

    public void removePlayerCard(Player player, int index) {
        if (player.getID() == 1) {
            PlayerCards1[index].setIcon(null);
        } else if (player.getID() == 2) {
            PlayerCards2[index].setIcon(null);
        }
    }

    /**
     * <b>Transformer(mutative)</b>: when the user selects a card to play (or rejects a card), card is replaced by another from the deck
     * <b>Precondition:</b> card is a card from the player's collection, cards the deck of cards
     * <b>Postcondition:</b> a new card is set at player's collection
     *
     * @param c card to be replaced
     * @param index card position
     * @param player
     */
    public void updateCard(Card c, int index, Player player) {
        URL imageURL = cldr.getResource(c.getImage());
        Image image = new ImageIcon(imageURL).getImage();
        image = image.getScaledInstance(80, 115, java.awt.Image.SCALE_SMOOTH);
        if (player.getID() == 1) {
            PlayerCards1[index].setIcon(new ImageIcon(image));
        } else if (player.getID() == 2) {
            PlayerCards2[index].setIcon(new ImageIcon(image));
        }
        tablo.repaint();
    }

    /**
     * <b>Accessor(selector):</b> Returns the player's collection of cards
     * <b>Precondition:</b> player is one of the 2 players
     * <b>Postcondition:</b> returns the player's collection of cards
     *
     * @param player the player whose cards will be returned
     * @return JButton[] the button cards
     */
    public JButton[] getPlayerCards(Player player) {
        if (player.getID() == 1) {
            return PlayerCards1;
        } else if (player.getID() == 2) {
            return PlayerCards2;
        }
        return null;
    }

    /**
     * <b>Accessor(selector):</b> Returns the deck of cards
     *
     * @return JButton the button back card
     */
    public JButton getCardsDeck() {
        return CardStack;
    }

    public JMenu getNewGameItem() {
        return item1;
    }

    public JMenu getExitItem() {
        return item2;
    }

    public JButton getFrescoBut1() {
        return FrescoButton1;
    }

    public JButton getFrescoBut2() {
        return FrescoButton2;
    }

    /**
     * <b>Transformer(mutative):</b> Sets the last card played from player on the appropriate palace position
     * <b>Precondition:</b> player is one of the 2 players, card is a card from his/her collection
     * <b>Postcondition:</b> last card played is set on the appropriate palace position
     *
     * @param player player who played the card
     * @param c the last card played
     */
    public void setLastCard(Player player, Card c) {
        URL imageURL = cldr.getResource(c.getImage());
        Image image = new ImageIcon(imageURL).getImage();
        image = image.getScaledInstance(80, 115, java.awt.Image.SCALE_SMOOTH);
        if (player.getID() == 1) {
            if (c.getPalace() == Knossos) {
                LastChoice1[0].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Malia) {
                LastChoice1[1].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Faistos) {
                LastChoice1[2].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Zakros) {
                LastChoice1[3].setIcon(new ImageIcon(image));
            }
        } else if (player.getID() == 2) {
            if (c.getPalace() == Knossos) {
                LastChoice2[0].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Malia) {
                LastChoice2[1].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Faistos) {
                LastChoice2[2].setIcon(new ImageIcon(image));
            }
            if (c.getPalace() == Zakros) {
                LastChoice2[3].setIcon(new ImageIcon(image));
            }
        }
    }

    public int choosePawn() {
        Object[] options = {"Θησέας", "Αρχαιολόγος"};
        int n = JOptionPane.showOptionDialog(this,
                "Επέλεξε πιόνι:", "Εξερευνάς νέο μονοπάτι",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        return n;
    }

    /**
     * <b>Transformer(mutative):</b> sets the pawn on its new position, updates available pawns if necessary
     * <b>Precondition:</b> player is one of the 2 players, position is the new position of pawn
     * <b>Postcondition:</b> the pawn is set on its new position
     *
     * @param player one of the 2 players
     * @param pawnIndex
     * @param palace the new position of pawn at path
     */
    public void updatePawn(Player player, int pawnIndex, Palace palace, int position) {
        if (player.getID() == 1) {
            Pawn1[pawnIndex].setBounds(5, 7, 40, 60);
            if (palace == Knossos) {
                KnossosPath[position].add(Pawn1[pawnIndex], 0);
            } else if (palace == Faistos) {
                FaistosPath[position].add(Pawn1[pawnIndex], 0);
            } else if (palace == Malia) {
                MaliaPath[position].add(Pawn1[pawnIndex], 0);
            } else if (palace == Zakros) {
                ZakrosPath[position].add(Pawn1[pawnIndex], 0);
            }
            tablo.repaint();
        } else if (player.getID() == 2) {
            Pawn2[pawnIndex].setBounds(45, 7, 40, 60);
            if (palace == Knossos) {
                KnossosPath[position].add(Pawn2[pawnIndex], 0);
            } else if (palace == Faistos) {
                FaistosPath[position].add(Pawn2[pawnIndex], 0);
            } else if (palace == Malia) {
                MaliaPath[position].add(Pawn2[pawnIndex], 0);
            } else if (palace == Zakros) {
                ZakrosPath[position].add(Pawn2[pawnIndex], 0);
            }
            tablo.repaint();
        }
    }

    /**
     * <b>Transformer(mutative):</b> sets the image of the rare finding that player discovered
     * <b>Precondition:</b> player is the owner of the finding, rare is the current finding
     * <b>Postcondition:</b> the image of discovered rare finding is set
     *
     * @param player the owner of the finding
     * @param palace
     */
    public void updateRareFindings(Player player, Palace palace) {

        ImageIcon ring = new ImageIcon(cldr.getResource("images/findings/ring.jpg"));

        if (palace == Knossos) {
            showInfoMessage("<html>Το «Δαχτυλίδι του Μίνωα», ένα από τα μεγαλύτερα και σπανιότερα "
                    + "<br>χρυσά σφραγιδικά στον κόσμο, θεωρείται από τα καλύτερα δείγματα της "
                    + "<br>κρητομυκηναϊκής σφραγιδικής. Φέρει σύνθετη θρησκευτική παράσταση, που "
                    + "<br>απεικονίζει μορφές οι οποίες εντάσσονται στην κρητομυκηναϊκή θεματολογία,"
                    + " <br>δεντρολατρία με καθιστή θεά, ουρανό, γη και θάλασσα, με ιερό πλοίο που "
                    + "<br>έχει μορφή ιππόκαμπου.", "Ανακάλυψες το Δαχτυλίδι του Μίνωα !!!", ring);

            Image image = ring.getImage(); //transform it 
            Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); //scale it the smooth way
            ring = new ImageIcon(newimg);  //transform it back

            if (player.getID() == 1) {
                FindingImage1[0].setIcon(ring);
            } else if (player.getID() == 2) {
                FindingImage2[0].setIcon(ring);
            }
        } else if (palace == Faistos) {

            ImageIcon diskos = new ImageIcon(cldr.getResource("images/findings/diskos.jpg"));

            showInfoMessage("<html>Ο Δίσκος της Φαιστού είναι ένα αρχαιολογικό εύρημα από τη Μινωική πόλη"
                    + " <br>της Φαιστού στη νότια Κρήτη και χρονολογείται πιθανώς στον 17ο αιώνα π.Χ.. "
                    + "<br>Αποτελεί ένα από τα γνωστότερα μυστήρια της αρχαιολογίας, αφού ο σκοπός της "
                    + "<br>κατασκευής του και το νόημα των όσων αναγράφονται σε αυτόν παραμένουν άγνωστα."
                    + " <br>Ο δίσκος ανακαλύφθηκε στις 3 Ιουλίου 1908 από τον Ιταλό αρχαιολόγο Λουίτζι "
                    + "<br>Περνιέ (Luigi Pernier) και φυλάσσεται σήμερα στο Αρχαιολογικό Μουσείο Ηρακλείου.",
                    "Ανακάλυψες το Δίσκο της Φαιστού!!!", diskos);

            Image image = diskos.getImage(); //transform it 
            Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); //scale it the smooth way
            diskos = new ImageIcon(newimg);  //transform it back

            if (player.getID() == 1) {
                FindingImage1[2].setIcon(diskos);
            } else if (player.getID() == 2) {
                FindingImage2[2].setIcon(diskos);
            }

        } else if (palace == Malia) {

            ImageIcon kosmima = new ImageIcon(cldr.getResource("images/findings/kosmima.jpg"));

            showInfoMessage("<html>Το χρυσό κόσμημα των μελισσών που φιλοξενείται στο Αρχαιολογικό Μουσείο "
                    + "<br>Ηρακλείου, είναι διάσημο αρχαιολογικό εύρημα από τον Χρυσόλακκο, τον ταφικό "
                    + "<br>περίβολο της νεκρόπολης των Μαλίων.", "Ανακάλυψες το Κόσμημα των Μαλίων!!!", kosmima);

            Image image = kosmima.getImage(); //transform it 
            Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); //scale it the smooth way
            kosmima = new ImageIcon(newimg);  //transform it back

            if (player.getID() == 1) {
                FindingImage1[1].setIcon(kosmima);
            } else if (player.getID() == 2) {
                FindingImage2[1].setIcon(kosmima);
            }
        } else if (palace == Zakros) {

            ImageIcon ruto = new ImageIcon(cldr.getResource("images/findings/ruto.jpg"));

            showInfoMessage("<html>Το αγγείο βρέθηκε στο θησαυροφυλάκιο του ανακτόρου της Ζάκρου μαζί με άλλα"
                    + " <br>μοναδικά στο είδος τους τελετουργικά σκεύη της νεοανακτορικής εποχής και αποτελεί"
                    + " <br>χαρακτηριστικό παράδειγμα της εφευρετικότητας και καλαισθησίας των Μινωιτών τεχνιτών.",
                    "Ανακάλυψες το Ρυτό της Ζάκρου!!!", ruto);

            Image image = ruto.getImage(); //transform it 
            Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); //scale it the smooth way
            ruto = new ImageIcon(newimg);  //transform it back

            if (player.getID() == 1) {
                FindingImage1[3].setIcon(ruto);
            } else if (player.getID() == 2) {
                FindingImage2[3].setIcon(ruto);
            }
        }

    }

    /**
     * <b>Transformer(mutative):</b> sets the image of fresco at player's collection
     * <b>Precondition:</b> player is the owner of the finding, fresco is the current finding
     * <b>Postcondition:</b> the image of discovered fresco is set
     *
     * @param player the owner of the finding
     * @param fresco the finding that player discovered
     */
    public void updateFresco(Player player, Fresco fresco) {

        if ("images/findings/fresco1_20.jpg".equals(fresco.getImage())) {
            ImageIcon fresco1 = new ImageIcon(cldr.getResource("images/findings/fresco1_20.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Οι γαλάζεις κυρίες!!!", fresco1);
            if (player.getID() == 1) {
                fresco1Label1.setIcon(fresco1);
            } else if (player.getID() == 2) {
                fresco1Label2.setIcon(fresco1);
            }
        } else if ("images/findings/fresco2_20.jpg".equals(fresco.getImage())) {
            ImageIcon fresco2 = new ImageIcon(cldr.getResource("images/findings/fresco2_20.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Τα ταυροκαθάψια!!!", fresco2);
            if (player.getID() == 1) {
                fresco2Label1.setIcon(fresco2);
            } else if (player.getID() == 2) {
                fresco2Label2.setIcon(fresco2);
            }
        } else if ("images/findings/fresco3_15.jpg".equals(fresco.getImage())) {
            ImageIcon fresco3 = new ImageIcon(cldr.getResource("images/findings/fresco3_15.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Τα δελφίνια!!!", fresco3);
            if (player.getID() == 1) {
                fresco3Label1.setIcon(fresco3);
            } else if (player.getID() == 2) {
                fresco3Label2.setIcon(fresco3);
            }
        } else if ("images/findings/fresco4_20.jpg".equals(fresco.getImage())) {
            ImageIcon fresco4 = new ImageIcon(cldr.getResource("images/findings/fresco4_20.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Ο πρίγκιπας με τα κρίνα", fresco4);
            if (player.getID() == 1) {
                fresco4Label1.setIcon(fresco4);
            } else if (player.getID() == 2) {
                fresco4Label2.setIcon(fresco4);
            }
        } else if ("images/findings/fresco5_15.jpg".equals(fresco.getImage())) {
            ImageIcon fresco5 = new ImageIcon(cldr.getResource("images/findings/fresco5_15.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Πομπή νέων!!!", fresco5);
            if (player.getID() == 1) {
                fresco5Label1.setIcon(fresco5);
            } else if (player.getID() == 2) {
                fresco5Label2.setIcon(fresco5);
            }
        } else if ("images/findings/fresco6_15.jpg".equals(fresco.getImage())) {
            ImageIcon fresco6 = new ImageIcon(cldr.getResource("images/findings/fresco6_15.jpg"));
            showInfoMessage(fresco.getInfo(), "Φωτογράφισες την Τοιχογραφία: Η παριζιάνα!!!", fresco6);
            if (player.getID() == 1) {
                fresco6Label1.setIcon(fresco6);
            } else if (player.getID() == 2) {
                fresco6Label2.setIcon(fresco6);
            }
        }
    }

    /**
     * Opens the window with the current player's fresco paintings
     *
     * @param player the player
     */
    public void OpenFrescoWindow(Player player) {
        
        if (player.getID() == 1) {
            imgPanel1 = new JPanel();
            imgPanel1.setLayout(new GridLayout(3, 3, 5, 5));
            imgPanel1.add(fresco1Label1);
            imgPanel1.add(fresco2Label1);
            imgPanel1.add(fresco3Label1);
            imgPanel1.add(fresco4Label1);
            imgPanel1.add(fresco5Label1);
            imgPanel1.add(fresco6Label1);

            frame1 = new JFrame();
            frame1.setTitle("Τοιχογραφίες Παίκτη1");
            frame1.add(imgPanel1);

            frame1.setBounds(1330, 50, 400, 460);
            frame1.setResizable(true);
            frame1.setVisible(true);
            
        } else if (player.getID() == 2) {
            imgPanel2 = new JPanel();
            imgPanel2.setLayout(new GridLayout(3, 3, 5, 5));
            imgPanel2.add(fresco1Label2);
            imgPanel2.add(fresco2Label2);
            imgPanel2.add(fresco3Label2);
            imgPanel2.add(fresco4Label2);
            imgPanel2.add(fresco5Label2);
            imgPanel2.add(fresco6Label2);

            frame2 = new JFrame();
            frame2.setTitle("Τοιχογραφίες Παίκτη2");
            frame2.add(imgPanel2);

            frame2.setBounds(1330, 450, 400, 460);
            frame2.setResizable(true);
            frame2.setVisible(true);
        }
    }

    /**
     * <b>Transformer(mutative):</b> Updates number of available cards, check points and players turn
     * <b>Postcondition:</b> number of available cards, check points and turn updated
     *
     * @param message the message that will be shown
     */
    public void updateInfobox(String message) {
        infobox.setText(message);
        tablo.repaint();
    }

    public void updateAvailablePawns(Player player) {
        int arch = player.getAvailableArcheologists();
        if (player.getID() == 1) {
            if (player.hasAvailableTheseus()) {
                AvailablePawns1.setText("Παίκτης1" + " - Διαθέσιμα Πιόνια: " + arch + " Αρχαιολόγοι και 1 Θησέας");
            } else {
                if (arch == 0) {
                    AvailablePawns1.setText("Παίκτης1" + " - Διαθέσιμα Πιόνια: 0");
                } else {
                    AvailablePawns1.setText("Παίκτης1" + " - Διαθέσιμα Πιόνια: " + arch + " Αρχαιολόγοι");
                }
            }
        } else if (player.getID() == 2) {
            if (player.hasAvailableTheseus()) {
                AvailablePawns2.setText("Παίκτης2" + " - Διαθέσιμα Πιόνια: " + arch + " Αρχαιολόγοι και 1 Θησέας");
            } else {
                if (arch == 0) {
                    AvailablePawns2.setText("Παίκτης2" + " - Διαθέσιμα Πιόνια: 0");
                } else {
                    AvailablePawns2.setText("Παίκτης2" + " - Διαθέσιμα Πιόνια: " + arch + " Αρχαιολόγοι");
                }
            }
        }
    }

    public void setPawnFinished(Palace palace) {

        if (palace == Knossos) {
            ImageIcon kp = new ImageIcon(cldr.getResource("images/paths/knossosPalace.jpg"));

            showInfoMessage("<html>Το μινωικό ανάκτορο είναι ο κύριος επισκέψιμος χώρος της Κνωσού (ή Κνωσσού),"
                    + " <br>σημαντικής πόλης κατά την αρχαιότητα, με συνεχή ζωή από τα νεολιθικά χρόνια έως "
                    + "<br>τον 5ο αι. Είναι χτισμένο στο λόφο της Κεφάλας, με εύκολη πρόσβαση στη θάλασσα αλλά "
                    + "<br>και στο εσωτερικό της Κρήτης. Κατά την παράδοση, υπήρξε η έδρα του σοφού βασιλιά Μίνωα."
                    + " <br>Συναρπαστικοί μύθοι, του Λαβύρινθου με το Μινώταυρο και του Δαίδαλου με τον Ίκαρο, "
                    + "<br>συνδέονται με το ανάκτορο της Κνωσσού.", "Έφτασες στο Ανάκτορο της Κνωσού!!!", kp);

        } else if (palace == Faistos) {
            ImageIcon fp = new ImageIcon(cldr.getResource("images/paths/phaistosPalace.jpg"));

            showInfoMessage("<html>Το Μινωικό Ανάκτορο της Φαιστού  βρίσκεται στην νότιο-κεντρική Κρήτη, στην "
                    + "<br>πεδιάδα της Μεσαράς, 55 χιλιόμετρα νότια από το Ηράκλειο και σε μικρή απόσταση από "
                    + "<br>τον αρχαιολογικό χώρο στην Αγία Τριάδα, τον αρχαιολογικό χώρο στη Γόρτυνα και τα "
                    + "<br>Μάταλα. Το μινωικό ανάκτορο της Φαιστού αντιστοιχεί σε ακμαία πόλη που, όχι τυχαία, "
                    + "<br>αναπτύχθηκε στην έφορη πεδιάδα της Μεσαράς κατά τους προϊστορικούς χρόνους, δηλαδή από"
                    + " <br>το 6.000 π.Χ. περίπου μέχρι και τον 1ο π.Χ. αιώνα, όπως επιβεβαιώνουν τα αρχαιολογικά"
                    + " <br>ευρήματα.", "Έφτασες στο Ανάκτορο της Φαιστού!!!", fp);

        } else if (palace == Malia) {
            ImageIcon mp = new ImageIcon(cldr.getResource("images/paths/maliaPalace.jpg"));

            showInfoMessage("<html>Ανατολικά από τα σημερινά Μάλια βρίσκεται το μινωικό ανάκτορο των Μαλίων. "
                    + "<br>Είναι το τρίτο σε μέγεθος ανάκτορο της μινωικής Κρήτης και είναι χτισμένο σε μια "
                    + "<br>τοποθεσία προνομιακή, κοντά στη θάλασσα και πάνω στο δρόμο που συνδέει την ανατολική"
                    + " <br>με την κεντρική Κρήτη. Το ανάκτορο των Μαλίων κατά τη μυθολογία χρησίμευε σαν "
                    + "<br>κατοικία του Σαρπηδόνα, αδερφού του Μίνωα, και πρωτοχτίζεται το 1900 π.Χ. Ο προϋπάρχων"
                    + " <br>ισχυρός οικισμός, από τον οποίο σώζονται συνοικίες γύρω από το ανάκτορο, μετατρέπεται"
                    + " <br>έτσι σε ανακτορικό κέντρο-πόλη.", "Έφτασες στο Ανάκτορο των Μαλίων!!!", mp);

        } else if (palace == Zakros) {
            ImageIcon zp = new ImageIcon(cldr.getResource("images/paths/zakrosPalace.jpg"));

            showInfoMessage("<html>Το ανάκτορο της Ζάκρου είναι το τέταρτο σε μέγεθος της Μινωικής Κρήτης. Βρισκόταν"
                    + " <br>σε σημαντικό στρατηγικό σημείο, σε ασφαλισμένο κολπίσκο, και ήταν κέντρο εμπορικών "
                    + "<br>ανταλλαγών με τις χώρες της Ανατολής, όπως φαίνεται από τα ευρήματα (χαυλιόδοντες ελέφαντα,"
                    + " <br>φαγεντιανή, χαλκός κλπ).  Το ανάκτορο αποτέλεσε το κέντρο διοίκησης, θρησκείας και εμπορίου."
                    + " <br>Το περιστοίχιζε η πόλη. Στο χώρο δεν έγινε νέα οικοδόμηση, εκτός από κάποιες καλλιέργειες.",
                    "Έφτασες στο Ανάκτορο της Ζάκρου!!!", zp);
        }
    }

    /**
     * <b>Transformer(mutative):</b> increases the number of statues of the player
     * <b>Precondition:</b> player is the owner of the finding
     * <b>Postcondition:</b> number of statues increased
     *
     * @param player the owner of the finding
     * @param number the updated number of statues
     */
    public void updateStatues(Player player, int number) {

        ImageIcon goddess = new ImageIcon(cldr.getResource("images/findings/snakes.jpg"));

        showInfoMessage("<html>Ως η θεά με τα φίδια ονομάζεται ο τύπος αγαλματίδιου που βρέθηκε σε ανασκαφές "
                + "<br>στους Μινωικούς αρχαιολογικούς τόπους που παρουσιάζει γυναίκα να κρατάει φίδια. Τα "
                + "<br>αγαλματίδα χρονολογούνται στον 16ο αιώνα π.Χ.. Λίγες πληροφορίες έχουμε για την ερμηνεία"
                + " <br>των αγαλματιδίων. Ο Έβανς συνδέει την θεά των όφεων με την Αιγυπτιακή θεά Ουατζέτ. "
                + "<br>Είναι προπομπός της κρητικής Ρέας και παρουσιάζει μεγάλη ομοιότητα με την φρυγική Κυβέλη "
                + "<br>και την εφεσία Αρτέμιδα.", "Βρήκες ένα άγαλμα της Θεάς των Φιδιών!!!", goddess);

        if (player.getID() == 1) {
            Statues1.setText("Αγαλματάκια: " + number);
        } else if (player.getID() == 2) {
            Statues2.setText("Αγαλματάκια: " + number);
        }
    }

    /**
     * <b>Transformer(mutative):</b> sets the new score of the player
     * <b>Precondition:</b> player is one of the 2 players and points his/her points
     * <b>Postcondition:</b> score updated
     *
     * @param player current player
     * @param points player's points
     */
    public void updateScore(Player player, int points) {
        if (player.getID() == 1) {
            Score1.setText("Το Σκορ μου: " + points + " Πόντοι");
        } else if (player.getID() == 2) {
            Score2.setText("Το Σκορ μου: " + points + " Πόντοι");
        }
    }

    /**
     * <b>Postcondition:</b> Information about the game is shown
     *
     * @param message information about the finding
     */
    public void showDialogMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    /**
     * Information Message appears when a player makes an excavation - discovers a finding
     * <b>Postcondition:</b> Information about the finding is shown
     *
     * @param message information about the finding
     * @param title the title
     * @param icon
     */
    public void showInfoMessage(String message, String title, ImageIcon icon) {
        JOptionPane.showMessageDialog(this, new JLabel(message, icon, JLabel.LEFT), title, JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Winning Message appears at the end of the game
     * <b>Postcondition:</b> the winner is announced
     *
     * @param message the winning message
     */
    public void showWinningMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }
}
