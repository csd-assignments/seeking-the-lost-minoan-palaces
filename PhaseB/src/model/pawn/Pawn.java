package model.pawn;

import model.palace.Palace;
import model.player.Player;

/**
 * Represents a pawn at the board. Cannot create an object of this class.
 *
 * @version 1.0
 * @author marianna vel
 */
public abstract class Pawn {

    private int current_position;
    private Palace palace;
    private int score;
    private Player owner;
    private boolean has_finished;

    public Pawn() {
        this.current_position = -1;
        this.has_finished = false;
        this.score = 0;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the current position of the pawn<br />
     * <b>Precondition:</b> parameter pos is a valid position (the new one)
     * <b>Postcondition:</b> the current position of the pawn is set </p>
     *
     * @param pos a valid position
     */
    public void setPosition(int pos) {
        this.current_position = pos;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the current position of the pawn <br/>
     * <b>Postcondition:</b> returns the current position of the pawn
     *
     * @return the current position of the pawn
     */
    public int getPosition() {
        return current_position;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the new score of a pawn
     * <b>Precondition:</b> parameter score is a positive or negative integer
     *
     * @param score the current points
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the score of a pawn
     * <b>Postcondition:</b> Returns the points a pawn has gathered
     *
     * @return the points of a player
     */
    public int getScore() {
        return score;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the palace(path) of the pawn<br />
     *
     * @param palace
     */
    public void setPalace(Palace palace) {
        this.palace = palace;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the palace of the pawn <br/>
     *
     * @return palace
     */
    public Palace getPalace() {
        return palace;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the owner of the pawn<br />
     *
     * @param owner
     */
    public void setOwner(Player owner) {
        this.owner = owner;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the owner of the pawn <br/>
     *
     * @return owner
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * <b>Observer</b>: Checks if a pawn has started <br/>
     * <b>Postcondition:</b> returns true if the pawn has been into the game
     *
     * @return true if the pawn has started, false otherwise
     */
    public boolean hasStarted() {
        if (this.getPosition() >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Observer</b>: Checks if a pawn has reached the end of its path <br/>
     * <b>Postcondition:</b> returns true if the pawn has reached the end of its path
     *
     * @return true if the pawn has reached the end of its path, false otherwise
     */
    public boolean hasFinished() {
        if (this.getPosition() == 8) {
            return true;
        } else {
            return false;
        }
    }

}
