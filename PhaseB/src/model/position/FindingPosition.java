package model.position;

import model.finding.Finding;
import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class FindingPosition extends Position {

    private Finding finding;

    /**
     * <b>Constructor:</b> initialises palace, points and finding at this position
     *
     * @param num
     * @param palace
     * @param points
     * @param finding
     */
    public FindingPosition(int num, Palace palace, int points, Finding finding) {
        super(num, palace, points);
        this.finding = finding;
    }

    /**
     * <b>Accessor:</b> returns the finding that exists at this position
     * <b>Postcondition:</b> the finding has been returned
     *
     * @return the finding of the current position
     */
    public Finding getFinding() {
        return finding;
    }

    /**
     * <b>Transformer:</b> sets the finding at the current position
     * <b>Precondition:</b> finding is a rare, fresco or snake goddess
     * <b>Postcondition:</b> finding has been set
     *
     * @param finding
     */
    public void setFinding(Finding finding) {
        this.finding = finding;
    }
}
