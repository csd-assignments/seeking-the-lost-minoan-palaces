package model.position;

import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class SimplePosition extends Position {

    /**
     * <b>Constructor:</b> initialises palace and points of the simple position
     * @param palace
     * @param points 
     */
    public SimplePosition(int num, Palace palace, int points) {
        super(num, palace, points);
    }

}
