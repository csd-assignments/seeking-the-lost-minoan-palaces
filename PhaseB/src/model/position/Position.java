package model.position;

import model.finding.Finding;
import model.palace.Palace;
import model.pawn.Pawn;
import model.player.Player;

/**
 * Represents a position at a board's path. Cannot create an object of this class.
 *
 * @version 1.0
 * @author marianna vel
 */
public abstract class Position {

    private int number;
    private Palace palace;
    private int points;
    private boolean hasFinding;
    private boolean hasPawn1, hasPawn2;
    private Pawn pawn1, pawn2;

    /**
     * <b>Constructor:</b> initialises palace and points of the position
     *
     * @param palace
     * @param points
     */
    public Position(int num, Palace palace, int points) {
        this.number = num;
        this.palace = palace;
        this.points = points;
    }

    /**
     * <b>Accessor:</b> returns the number of the current position
     * <b>Postcondition:</b> the number has been returned
     *
     * @return a number from 0 to 8
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * <b>Transformer:</b> sets the number of this position
     * <b>Precondition:</b> number is an integer from 1 to 9
     * <b>Postcondition:</b> number of position is set
     *
     * @param number is an integer from 1 to 9
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * <b>Accessor:</b> returns the path of the palace where this position belongs
     * <b>Postcondition:</b> the palace has been returned
     *
     * @return the palace where this position belongs
     */
    public Palace getPalace() {
        return palace;
    }

    /**
     * <b>Transformer:</b> sets the palace where the path of this position leads
     * <b>Precondition:</b> palace is one of the: Knossos, Faistos, Malia, Zakros
     * <b>Postcondition:</b> path of position has been identified
     *
     * @param palace one of the 4 palaces
     */
    public void setPalace(Palace palace) {
        this.palace = palace;
    }

    /**
     * <b>Accessor:</b> returns the points of the current position
     * <b>Postcondition:</b> the points have been returned
     *
     * @return the points of the current position
     */
    public int getPoints() {
        return points;
    }

    /**
     * <b>Transformer:</b> sets the points of the current position
     * <b>Precondition:</b> points are a positive or negative integer
     * <b>Postcondition:</b> points of the position have been set
     *
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * <b>Transformer:</b> removes the finding from this position
     * <b>Postcondition:</b> finding has been removed

     */
    public void removeFinding() {
        hasFinding = false;
    }

    /**
     * <b>Transformer:</b> sets if the position has finding
     */
    public void sethasFinding(boolean option) {
        hasFinding = option;
    }

    /**
     * <b>Observer:</b> checks if the current position has finding
     * <b>Postcondition:</b> Returns true if position has available finding
     *
     * @return true if it does, false otherwise
     */
    public boolean hasFinding() {
        if (this.getNumber() % 2 != 0 || this.getNumber() == 8) {
            if (hasFinding == true) {
                return true;
            }
        }
        return false;
    }

    /**
     * <b>Accessor:</b> Returns the pawn at this position
     *
     * @return the pawn at this position
     */
    public Pawn getPawn(Player player) {
        if (player.getID() == 1) {
            return pawn1;
        } else if (player.getID() == 2) {
            return pawn2;
        }
        return null;
    }

    /**
     * <b>Transformer:</b> Sets the pawn at this position
     *
     * @param pawn the pawn to be set
     */
    public void setPawn(Player player, Pawn pawn) {

        if (player.getID() == 1) {
            hasPawn1 = true;
            this.pawn1 = pawn;
        } else if (player.getID() == 2) {
            hasPawn2 = true;
            this.pawn2 = pawn;
        }
    }

    /**
     * Removes the pawn from this position
     */
    public void removePawn(Player player) {
        if (player.getID() == 1) {
            hasPawn1 = false;
            this.pawn1 = null;
        } else if (player.getID() == 2) {
            hasPawn2 = false;
            this.pawn2 = null;
        }
    }

    /**
     * <b>Observer:</b> checks if the current position has pawn
     * <b>Postcondition:</b> Returns true if position has pawn
     *
     * @return true if it does, false otherwise
     */
    public boolean hasPawn(Player player) {
        if (player.getID() == 1) {
            if (hasPawn1 == true) {
                return true;
            }
        }
        if (player.getID() == 2) {
            if (hasPawn2 == true) {
                return true;
            }
        }
        return false;
    }
}
