package model.player;

import model.card.*;
import model.pawn.Pawn;
import model.position.Position;

/**
 * Specification of an ADT for a player of a table game with cards and pawns. 
 * The implementation of this interface's functions is going to be used for a 
 * GUI application that allows the user to become player of a table game.
 *
 * @version 1.0
 * @author marianna vel
 */
public interface PlayerADT {

    /**
     * <b>Transformer(mutative)</b>: Valuates the cardforplaying. 
     * - Sets the card that a player wants to play to this palace path. 
     * - Removes the card from player's collection.
     *
     * @param cardforplaying the new card that player wants to play
     */
    public void playCard(NumberCard cardforplaying);
    
    /**
     * <b>Transformer(mutative):</b> Depending on the card moves the pawn steps front or back
     * <b>Preconditions:</b> pawn is a valid pawn, position a valid position or null
     * <b>Postcondition:</b> the pawn is moved at new position
     *
     * @param pawn the pawn that will be moved
     * @param card the card was played
     */
    public void movePawn(Pawn pawn, Card card);

}
