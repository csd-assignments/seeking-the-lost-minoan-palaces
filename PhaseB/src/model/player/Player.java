package model.player;

import java.util.ArrayList;
import model.card.*;
import model.finding.*;
import model.palace.Palace;
import static model.palace.Palace.*;
import model.pawn.*;

/**
 * Player class describes the characteristics of a player and provides modification methods.
 *
 * @version 1.0
 * @author marianna vel
 */
public class Player implements PlayerADT {

    private String name;
    private Card[] cards;

    private ArrayList<RareFinding> rares;
    private ArrayList<Fresco> frescos;
    private int goddess_num;

    private Pawn[] pawns;
    private int ID; //0, 1 or 2
    private int positionScore, findingScore;
    //listes apo tis number kartes pou exoun paixtei
    private ArrayList<NumberCard> PlayedKnossos;
    private ArrayList<NumberCard> PlayedFaistos;
    private ArrayList<NumberCard> PlayedMalia;
    private ArrayList<NumberCard> PlayedZakros;

    /**
     * <b>Constructor</b>: Constructs a new Player with no name.<br />
     * <b>Postcondition</b>: Creates and initializes a player with no name.
     */
    public Player() {
        this("Anonymous", 0);
    }

    /**
     * <b>Constructor</b>: Constructs a new Player with the given parameter name.<br />
     * <b>Postcondition</b>: Creates and initializes a player with the given name. Also initializes their cards collection and their choice to 0. They have neither played nor finished at the beginning of the game.
     *
     * @param name is the name of the player.
     * @param ID the player id
     */
    public Player(String name, int ID) {
        this.name = name;
        this.ID = ID;
        cards = new Card[8];
        rares = new ArrayList<>();
        frescos = new ArrayList<>();
        pawns = new Pawn[4];
        PlayedKnossos = new ArrayList<>();
        PlayedFaistos = new ArrayList<>();
        PlayedMalia = new ArrayList<>();
        PlayedZakros = new ArrayList<>();
        goddess_num = 0;
        positionScore = 0;
        findingScore = 0;
    }

    /**
     * <b>Transformer(mutative)</b>: It initializes a player for a new game <br />
     * <b>Postcondition:</b> initializes a player for a new game
     */
    public void init_player() {
        cards = new Card[8];
        rares = new ArrayList<RareFinding>();
        frescos = new ArrayList<Fresco>();
        pawns = new Pawn[4];
        PlayedKnossos = new ArrayList<>();
        PlayedFaistos = new ArrayList<>();
        PlayedMalia = new ArrayList<>();
        PlayedZakros = new ArrayList<>();
        goddess_num = 0;
        positionScore = 0;
        findingScore = 0;
    }

    public void initPawns() {
        pawns[0] = new Theseus();
        for (int i = 1; i < 4; i++) {
            pawns[i] = new Archeologist();
        }
    }

    /**
     * <b>Accessor(selector)</b>:Returns the name of the player <br/>
     * <p>
     * <b>Postcondition:</b> returns the name of the player </p>
     *
     * @return the name of the player
     */
    public String getName() {
        return name;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the name of the player to name <br />
     * <p>
     * <b>Postcondition:</b> the name of this player is set as name</p>
     *
     * @param name the name of the player
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the ID of a player. <br />
     * <b>Postcondition</b>: Returns the ID of the player (1 / 2)
     *
     * @return the ID of the player (1 / 2)
     */
    public int getID() {
        return ID;
    }

    /**
     * <b>Transformer(mutative)</b>: It sets the ID of a player <br />
     * <b>Postcondition</b>:the ID of the player is changed to id
     *
     * @param id the new ID of the player
     */
    public void setID(int id) {
        this.ID = id;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the total position score of a player
     */
    public void setPositionScore() {
        int theseusScore = 2 * pawns[0].getScore();
        int archScore = 0;
        for (int i = 1; i < 4; i++) {
            archScore += pawns[i].getScore();
        }
        positionScore = archScore + theseusScore;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the position score
     *
     * @return the position score
     */
    public int getPositionScore() {
        return positionScore;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the score from all the player's findings
     *
     * @return the score from all the player's findings
     */
    public int getFindingScore() {
        findingScore = 0;
        if (!rares.isEmpty()) {
            for (int i = 0; i < rares.size(); i++) {
                findingScore += rares.get(i).getPoints();
            }
        }
        if (!frescos.isEmpty()) {
            for (int i = 0; i < frescos.size(); i++) {
                findingScore += frescos.get(i).getPoints();
            }
        }
        int statuePoints;
        switch (goddess_num) {
            case 1:
                statuePoints = -20;
                break;
            case 2:
                statuePoints = -15;
                break;
            case 3:
                statuePoints = 5;
                break;
            case 4:
                statuePoints = 15;
                break;
            case 5:
                statuePoints = 30;
                break;
            case 6:
                statuePoints = 45;
                break;
            case 7:
                statuePoints = 55;
                break;
            case 8:
                statuePoints = 70;
                break;
            case 9:
                statuePoints = 90;
                break;
            case 10:
                statuePoints = 100;
                break;
            default:
                statuePoints = 0;
        }
        findingScore += statuePoints;
        return findingScore;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the score of a player
     * <b>Postcondition:</b> Returns the total number of points a player has gathered
     *
     * @return the points of a player
     */
    public int getTotalScore() {
        return getPositionScore() + getFindingScore();
    }

    /**
     * <b>Accessor(selector)</b>:Returns a card from the player's collection <br />
     * <b>Postcondition:</b> returns a card from the player's collection </p>
     *
     * @param index the position of the card at player's collection of cards
     * @return a card from the player's collection of cards
     */
    public Card getCard(int index) {
        return cards[index];
    }

    /**
     * <b>Transformer(mutative)</b>: adds a Card to player's cards  <br />
     * <b>Postcondition:</b> a card is added to players cards</p>
     *
     * @param c the card that will be added to players collection
     * @param index the position of card
     */
    public void setCard(Card c, int index) {
        cards[index] = c;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the player's collection of cards<br />
     * <b>Postcondition:</b> returns the player's collection of cards
     *
     * @return the player's collection of cards
     */
    public Card[] getCards() {
        return cards;
    }

    /**
     * <b>Transformer(mutative)</b>: removes a Card from player's cards
     * <b>Postcondition:</b> a card is removed from players cards
     *
     * @param index the position of card at player's array
     */
    public void removeCard(int index) {
        cards[index] = null;
    }

    /**
     * <b>Transformer(mutative)</b>: Valuates the cardforplaying.
     * <b>Sets the card that a player wants to play to this palace path.</b>
     *
     * @param cardforplaying the new card that player wants to play
     */
    public void playCard(NumberCard cardforplaying) {
        if (cardforplaying.getPalace() == Knossos) {
            PlayedKnossos.add(cardforplaying);
        } else if (cardforplaying.getPalace() == Faistos) {
            PlayedFaistos.add(cardforplaying);
        } else if (cardforplaying.getPalace() == Malia) {
            PlayedMalia.add(cardforplaying);
        } else if (cardforplaying.getPalace() == Zakros) {
            PlayedZakros.add(cardforplaying);
        }
        for (int i = 0; i < 8; i++) {
            if (cards[i] == cardforplaying) {
                cards[i] = null;
                break;
            }
        }
    }

    /**
     * <b>Accessor(selector)</b>:Returns the last card that this player played at this palace
     * <b>Postcondition:</b> returns the last card that this player played at this palace
     *
     * @param palace
     * @return the last card that this player played at this palace
     */
    public Card getLastCardPlayed(Palace palace) {
        if (palace == Knossos) {
            if (PlayedKnossos.isEmpty() == false) {
                return PlayedKnossos.get(PlayedKnossos.size() - 1);
            }
        } else if (palace == Faistos) {
            if (PlayedFaistos.isEmpty() == false) {
                return PlayedFaistos.get(PlayedFaistos.size() - 1);
            }
        } else if (palace == Malia) {
            if (PlayedMalia.isEmpty() == false) {
                return PlayedMalia.get(PlayedMalia.size() - 1);
            }
        } else if (palace == Zakros) {
            if (PlayedZakros.isEmpty() == false) {
                return PlayedZakros.get(PlayedZakros.size() - 1);
            }
        }
        return null;
    }

    /**
     * <b>Transformer(mutative)</b>: sets a pawn at a path
     * <b>Postcondition:</b> a pawn is set at a path
     *
     * @param path the path of the pawn
     * @param pawn the updated pawn
     */
    public void setPawn(Palace path, Pawn pawn) {
        for (int i = 0; i < 4; i++) {
            if (pawns[i].getPalace() == path) {
                pawns[i] = pawn;
            }
        }
    }

    /**
     * <b>Accessor(selector)</b>:Returns the pawn at this palace
     * <b>Postcondition:</b> returns the pawn at this palace
     *
     * @param path the palace
     * @return the pawn at this palace
     */
    public Pawn getPawn(Palace path) {
        for (int i = 0; i < 4; i++) {
            if (pawns[i].getPalace() == path) {
                return pawns[i];
            }
        }
        return null;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the array of pawns
     *
     * @return array of pawns
     */
    public Pawn[] getPawns() {
        return pawns;
    }

    /**
     * <b>Transformer:</b> Depending on the card moves the pawn steps front or back
     * <b>Preconditions:</b> pawn is a valid pawn, position a valid position or null
     * <b>Postcondition:</b> the pawn is moved at new position
     *
     * @param pawn the pawn that will be moved
     * @param card the card that was played
     */
    public void movePawn(Pawn pawn, Card card) {
        int number = pawn.getPosition();
        if (card instanceof NumberCard && pawn.getPosition() < 9) {
            pawn.setPosition(number + 1);
        } else if (card instanceof Ariadne && pawn.getPosition() < 8) {
            if (number == 7) {
                pawn.setPosition(number + 1);
            } else {
                pawn.setPosition(number + 2);
            }
        } else if (card instanceof Minotaur) {
            if (pawn instanceof Archeologist) {
                if (number >= 2) {
                    if (number >= 6) {
                        return;
                    }
                    pawn.setPosition(number - 2);
                } else {
                    pawn.setPosition(0);
                }
            } else if (pawn instanceof Theseus) {
                return;
            }
        }
    }

    /**
     * <b>Accessor(selector)</b>:Returns the available archeologists
     *
     * @return the available archeologists
     */
    public int getAvailableArcheologists() {
        int count = 0;
        for (int i = 1; i < pawns.length; i++) {
            if (pawns[i].getPosition() == -1) {
                count++;
            }
        }
        return count;
    }

    /**
     * <b>Observer</b>: checks if player has available theseus pawn
     *
     * @return true of false
     */
    public boolean hasAvailableTheseus() {
        if (pawns[0].getPosition() == -1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param pawn
     * @return
     */
    public int findPawnIndex(Pawn pawn) {
        for (int i = 0; i < 4; i++) {
            if (pawns[i] == pawn) {
                return i;
            }
        }
        return -1;
    }

    /**
     * <b>Transformer(mutative)</b>: puts the finding at the appropriate ArrayList or increases the statues number
     * <b>Precondition:</b> parameter finding has to be a snakegoddess, a fresco or a rarefinding
     *
     * @param finding the finding at pawn's position
     */
    public void setFinding(Finding finding) {
        if (finding instanceof SnakeGoddess) {
            goddess_num++;
        } else if (finding instanceof RareFinding) {
            rares.add((RareFinding) finding);
        } else if (finding instanceof Fresco) {
            frescos.add((Fresco) finding);
        }
    }

    /**
     * <b>Accessor:</b> returns all the rare findings
     * <b>Postcondition:</b> all the rare findings have been returned
     *
     * @return all the rare findings
     */
    public ArrayList<RareFinding> getRareFindings() {
        return rares;
    }

    /**
     * <b>Accessor:</b> returns all the frescos
     * <b>Postcondition:</b> all the frescos has been returned
     *
     * @return all the frescos
     */
    public ArrayList<Fresco> getFrescos() {
        return frescos;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the total number of goddess statues from the player's collection <br />
     * <b>Postcondition:</b> returns the number of goddess statues from the player's collection </p>
     *
     * @return the number of godess statues of the current player
     */
    public int numberOfStatues() {
        return goddess_num;
    }

}
