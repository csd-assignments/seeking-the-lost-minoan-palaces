package model.finding;

/**
 * Contains the attributes that any finding has at this game. Cannot create an object of this class.
 *
 * @version 1.0
 * @author marianna vel
 */
public abstract class Finding {

    private int points;
    private String image;
    private String information;

    /**
     * <b>Constructor</b>: Constructs finding giving its points
     *
     * @param points the points of the finding
     */
    public Finding(int points) {
        this.points = points;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the points of this finding
     *
     * @return the points of the current finding
     */
    public int getPoints() {
        return points;
    }

    /**
     * <b>Transformer(mutative)</b>: sets to the finding its points
     *
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the information of the finding
     *
     * @return the information of the finding
     */
    public String getInfo() {
        return information;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the information of the finding
     *
     * @param info the information of the finding
     */
    public void setInfo(String info) {
        this.information = info;
    }
    
    /**
     * <b>Accessor:</b> returns the image of this finding
     * <b>Postcondition:</b> image has been returned
     *
     * @return image of finding
     */
    public String getImage() {
        return image;
    }

    /**
     * <b>Transformer:</b> sets the image of this finding
     * <b>Postcondition:</b> image has been set
     *
     * @param image the image of finding
     */
    public void setImage(String image) {
        this.image = image;
    }
}
