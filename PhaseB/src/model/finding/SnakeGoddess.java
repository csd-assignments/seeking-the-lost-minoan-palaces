package model.finding;

/**
 * @version 1.0
 * @author marianna vel
 */
public class SnakeGoddess extends Finding {

    /**
     * <b>Constructor</b>: Constructs snake goddess with no points
     */
    public SnakeGoddess() {
        super(0);
    }
}
