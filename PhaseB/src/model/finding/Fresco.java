package model.finding;

/**
 * @version 1.0
 * @author marianna vel
 */
public class Fresco extends Finding {

    /**
     * <b>Constructor</b>: Constructs fresco giving its points
     *
     * @param points the points of the fresco
     */
    public Fresco(int points) {
        super(points);
    }

}