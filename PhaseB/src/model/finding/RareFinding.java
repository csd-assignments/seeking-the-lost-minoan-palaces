package model.finding;

/**
 * @version 1.0
 * @author marianna vel
 */
public class RareFinding extends Finding{
    
    /**
     * <b>Constructor</b>: Constructs rare finding giving its points
     * @param points the points of the finding
     */
    public RareFinding(int points){
        super(points);
    }
}
