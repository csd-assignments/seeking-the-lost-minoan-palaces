package model.card;

import model.palace.Palace;

/**
 * Contains the methods needed for creating a number or special card for each one of the four palaces. Cannot create an object of this class.
 *
 * @version 1.0
 * @author marianna vel
 */
public abstract class Card {

    private Palace palace;
    String image;

    /**
     * Constructor: constructs a new card
     *
     * @param palace
     */
    public Card(Palace palace) {
        this.palace = palace;
    }

    /**
     * <b>Transformer:</b> sets the palace of this card
     * <b>Postcondition:</b> palace card has been identified
     *
     * @param palace
     */
    public void setPalace(Palace palace) {
        this.palace = palace;
    }

    /**
     * <b>Accessor:</b> returns the palace of this card
     * <b>Postcondition:</b> palace has been returned
     *
     * @return the palace of this card
     */
    public Palace getPalace() {
        return palace;
    }

    /**
     * <b>Accessor:</b> returns the image of this card
     * <b>Postcondition:</b> image has been returned
     *
     * @return image of card
     */
    public String getImage() {
        return image;
    }

    /**
     * <b>Transformer:</b> sets the image of this card
     * <b>Postcondition:</b> image has been set
     *
     * @param image the image of card
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * <b>Observer:</b> Checks if a card can be played above another
     * <b>Postcondition:</b> Returns true if this card matches parameter Card
     *
     * @param other the card to be matched
     * @return true if this card matches parameter Card
     */
    public boolean matchCard(NumberCard other) {
        if (this instanceof NumberCard) {
            NumberCard numCard = (NumberCard) this;
            if (numCard.getValue() >= other.getValue()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
