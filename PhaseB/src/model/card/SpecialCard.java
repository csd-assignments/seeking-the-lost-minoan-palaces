package model.card;

import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class SpecialCard extends Card {

    public SpecialCard(Palace palace){
        super(palace);
    }
}
