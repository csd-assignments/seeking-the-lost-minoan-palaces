
package model.card;

import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class Minotaur extends SpecialCard{
    
    public Minotaur(Palace palace){
        super(palace);
    }
}
