
package model.card;

import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class Ariadne extends SpecialCard{
    
    public Ariadne(Palace palace){
        super(palace);
    }
}
