package model.card;

import model.palace.Palace;

/**
 * @version 1.0
 * @author marianna vel
 */
public class NumberCard extends Card {

    private int value;

    public NumberCard(Palace palace) {
        super(palace);
    }

    /**
     * <b>Accessor:</b> returns the card's value (from 1-10)
     * <b>Postcondition:</b> card's value has been returned
     *
     * @return int value
     */
    public int getValue() {
        return this.value;
    }

    /**
     * <b>Transformer:</b> sets the card's value
     * <b>Precondition:</b> value has to be from 1 to 10
     * <b>Postcondition:</b> card's value has been set
     *
     * @param value
     */
    public void setValue(int value) {
        this.value = value;
    }
}
