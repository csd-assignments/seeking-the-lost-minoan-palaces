package model.palace;

/**
 * This enumeration represents the 4 palaces
 * @version 1.0
 * @author marianna vel
 */
public enum Palace {
    Knossos, Faistos, Malia, Zakros
}
