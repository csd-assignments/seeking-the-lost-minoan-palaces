package model.board;

import java.util.ArrayList;
import java.util.Collections;
import model.card.*;
import model.finding.*;
import model.palace.Palace;
import static model.palace.Palace.*;
import model.pawn.Pawn;
import model.player.Player;
import model.position.*;

/**
 * Represents the board of the table game.
 *
 * @version 1.0
 * @author marianna vel
 */
public class Board {

    private ArrayList<Card> CardsDeck;
    private Player turn;
    private int CheckPoints;

    private Position[] KnossosPath;
    private Position[] FaistosPath;
    private Position[] MaliaPath;
    private Position[] ZakrosPath;

    private RareFinding diskos, kosmima, ring, ruto;
    private ArrayList<Finding> simple_findings;
    private SnakeGoddess[] statues;
    private Fresco[] frescos;
    private ArrayList<Finding> KnossosFindings;
    private ArrayList<Finding> FaistosFindings;
    private ArrayList<Finding> MaliaFindings;
    private ArrayList<Finding> ZakrosFindings;

    private boolean GameOver;

    /**
     * <b>Constructor</b>: Constructs a new Board.<br />
     * <b>Postcondition</b>: Creates and initializes the cards deck and the 4 paths.
     */
    public Board() {
        CardsDeck = new ArrayList<Card>();
        KnossosPath = new Position[9];
        FaistosPath = new Position[9];
        MaliaPath = new Position[9];
        ZakrosPath = new Position[9];
        this.CheckPoints = 0;
        statues = new SnakeGoddess[10];
        frescos = new Fresco[6];
        this.GameOver = false;
    }

    public void initFindings() {

        diskos = new RareFinding(35);
        ring = new RareFinding(25);
        kosmima = new RareFinding(25);
        ruto = new RareFinding(25);

        //vazw ola ta agalmata kai tis toixografies se ena arraylist
        simple_findings = new ArrayList<Finding>();
        for (int i = 0; i < 10; i++) {
            statues[i] = new SnakeGoddess();
            simple_findings.add(statues[i]);
        }
        for (int i = 0; i < 6; i++) {
            if (i < 3) {
                frescos[i] = new Fresco(20);
            } else {
                frescos[i] = new Fresco(15);
            }
            simple_findings.add(frescos[i]);
        }
        /* edo arxikopoiw ta pedia tn toixografiwn gt tha xreiastoun sto view */
        frescos[0].setInfo("<html>Όμορφες Μινωίτισσες που κουβεντιάζουν. Έχουν ωραία φορέματα, σύμφωνα με τη "
                + "<br>μόδα της εποχής, όμορφα χτενισμένα μαλλιά και πολύτιμα κοσμήματα.");
        frescos[0].setImage("images/findings/fresco1_20.jpg");
        
        frescos[1].setInfo("<html>Τα ταυροκαθάψια ήταν ένα αγώνισμα που συνηθιζόταν πολύ στα μινωικά χρόνια. "
                + "<br>Περιλάμβανε το πιάσιμο του ταύρου από τα κέρατα, το επικίνδυνο άλμα στη ράχη του ζώου "
                + "<br>και τέλος το πήδημα στο έδαφος πίσω του.");
        frescos[1].setImage("images/findings/fresco2_20.jpg");
        
        frescos[2].setInfo("<html>Εικονίζεται επιβλητική ανδρική μορφή, που βαδίζει προς"
                + " <br>τα αριστερά σε απροσδιόριστο ερυθρό φόντο. Φοράει το τυπικό μινωικό περίζωμα με φαρδιά "
                + "<br>ζώνη, περιδέραιο στο λαιμό και πλούσιο κάλυμμα κεφαλής διακοσμημένο με κρίνα και φτερά "
                + "<br>παγωνιού. Η στάση των χεριών του δείχνει ότι ίσως έσερνε με το αριστερό του χέρι ένα ζώο "
                + "<br>ή κάποιο μυθικό τέρας, γρύπα ή σφίγγα. Ο νέος ονομάσθηκε από τους ερευνητές «πρίγκηπας», "
                + "<br>γιατί θεωρήθηκε ότι αποδίδει το βασιλιά-ιερέα, που ζούσε στο ανάκτορο της Κνωσού.!!!");
        frescos[2].setImage("images/findings/fresco4_20.jpg");
        
        frescos[3].setInfo("<html>Η τοιχογραφία αυτή προέρχεται από τo μέγαρο της βασίλισσας. Δελφίνια κολυμπούν "
                + "<br>ανάμεσα σε ψάρια, μέσα στα κύματα.");
        frescos[3].setImage("images/findings/fresco3_15.jpg");
        
        frescos[4].setInfo("<html>Νέοι λαμβάνουν μέρος σε θρησκευτική πομπή και φέρουν αγγεία με δώρα για τη θεότητα"
                + " <br>ή για το βασιλιά. Η τοιχογραφία αυτή κοσμούσε τον λεγόμενο «διάδρομο της πομπής» του "
                + "<br>ανακτόρου της Κνωσού.");
        frescos[4].setImage("images/findings/fresco5_15.jpg");
        
        frescos[5].setInfo("<html>Εικονίζεται μια γυναίκα αριστοκρατικής καταγωγής σε θέση προφίλ.  Ονομάστηκε "
                + "<br>«Παριζιάνα» από τον Άρθουρ Έβανς, γιατί το 1903 (έτος που ανακαλύφθηκε) τα μεγάλα μάτια, "
                + "<br>τα κατσαρά μαλλιά, τα έντονα κόκκινα χείλη και η ανασηκωμένη μύτη θεωρούνταν τα ιδεώδη της "
                + "<br>γυναικείας ομορφιάς, τα οποία μόνο μια όμορφη γυναίκα από … το Παρίσι μπορούσε να τα ενσαρκώσει.");
        frescos[5].setImage("images/findings/fresco6_15.jpg");
        
        Collections.shuffle(simple_findings);

        //ftiaxnw ksexwristo arraylist gia kathe anaktoro pou periexei ta findings tou
        KnossosFindings = new ArrayList<Finding>();
        FaistosFindings = new ArrayList<Finding>();
        MaliaFindings = new ArrayList<Finding>();
        ZakrosFindings = new ArrayList<Finding>();

        KnossosFindings.add(ring);
        FaistosFindings.add(diskos);
        MaliaFindings.add(kosmima);
        ZakrosFindings.add(ruto);
        for (int i = 0; i < 4; i++) {
            KnossosFindings.add(simple_findings.get(i));
            FaistosFindings.add(simple_findings.get(i + 4));
            MaliaFindings.add(simple_findings.get(i + 8));
            ZakrosFindings.add(simple_findings.get(i + 12));
        }
        Collections.shuffle(KnossosFindings);
        Collections.shuffle(FaistosFindings);
        Collections.shuffle(MaliaFindings);
        Collections.shuffle(ZakrosFindings);
        //twra exw 4 arraylists me 5 findings to kathena se tuxaia thesi
    }

    public void initPaths() {
        KnossosPath[0] = new SimplePosition(0, Knossos, -20);
        KnossosPath[1] = new FindingPosition(1, Knossos, -15, KnossosFindings.get(0));
        KnossosPath[2] = new SimplePosition(2, Knossos, -10);
        KnossosPath[3] = new FindingPosition(3, Knossos, 5, KnossosFindings.get(1));
        KnossosPath[4] = new SimplePosition(4, Knossos, 10);
        KnossosPath[5] = new FindingPosition(5, Knossos, 15, KnossosFindings.get(2));
        KnossosPath[6] = new SimplePosition(6, Knossos, 30);
        KnossosPath[7] = new FindingPosition(7, Knossos, 35, KnossosFindings.get(3));
        KnossosPath[8] = new FindingPosition(8, Knossos, 50, KnossosFindings.get(4));

        FaistosPath[0] = new SimplePosition(0, Faistos, -20);
        FaistosPath[1] = new FindingPosition(1, Faistos, -15, FaistosFindings.get(0));
        FaistosPath[2] = new SimplePosition(2, Faistos, -10);
        FaistosPath[3] = new FindingPosition(3, Faistos, 5, FaistosFindings.get(1));
        FaistosPath[4] = new SimplePosition(4, Faistos, 10);
        FaistosPath[5] = new FindingPosition(5, Faistos, 15, FaistosFindings.get(2));
        FaistosPath[6] = new SimplePosition(6, Faistos, 30);
        FaistosPath[7] = new FindingPosition(7, Faistos, 35, FaistosFindings.get(3));
        FaistosPath[8] = new FindingPosition(8, Faistos, 50, FaistosFindings.get(4));

        MaliaPath[0] = new SimplePosition(0, Malia, -20);
        MaliaPath[1] = new FindingPosition(1, Malia, -15, MaliaFindings.get(0));
        MaliaPath[2] = new SimplePosition(2, Malia, -10);
        MaliaPath[3] = new FindingPosition(3, Malia, 5, MaliaFindings.get(1));
        MaliaPath[4] = new SimplePosition(4, Malia, 10);
        MaliaPath[5] = new FindingPosition(5, Malia, 15, MaliaFindings.get(2));
        MaliaPath[6] = new SimplePosition(6, Malia, 30);
        MaliaPath[7] = new FindingPosition(7, Malia, 35, MaliaFindings.get(3));
        MaliaPath[8] = new FindingPosition(8, Malia, 50, MaliaFindings.get(4));

        ZakrosPath[0] = new SimplePosition(0, Zakros, -20);
        ZakrosPath[1] = new FindingPosition(1, Zakros, -15, ZakrosFindings.get(0));
        ZakrosPath[2] = new SimplePosition(2, Zakros, -10);
        ZakrosPath[3] = new FindingPosition(3, Zakros, 5, ZakrosFindings.get(1));
        ZakrosPath[4] = new SimplePosition(4, Zakros, 10);
        ZakrosPath[5] = new FindingPosition(5, Zakros, 15, ZakrosFindings.get(2));
        ZakrosPath[6] = new SimplePosition(6, Zakros, 30);
        ZakrosPath[7] = new FindingPosition(7, Zakros, 35, ZakrosFindings.get(3));
        ZakrosPath[8] = new FindingPosition(8, Zakros, 50, ZakrosFindings.get(4));

        for (int i = 0; i < 9; i++) {
            if (KnossosPath[i] instanceof FindingPosition) {
                KnossosPath[i].sethasFinding(true);
            }
            if (FaistosPath[i] instanceof FindingPosition) {
                FaistosPath[i].sethasFinding(true);
            }
            if (MaliaPath[i] instanceof FindingPosition) {
                MaliaPath[i].sethasFinding(true);
            }
            if (ZakrosPath[i] instanceof FindingPosition) {
                ZakrosPath[i].sethasFinding(true);
            }
        }
    }

    public Pawn getPawn(Player player, Palace palace) {
        if (palace == Knossos) {
            for (int i = 0; i < 8; i++) {
                if (KnossosPath[i].hasPawn(player)) {
                    return KnossosPath[i].getPawn(player);
                }
            }
        } else if (palace == Faistos) {
            for (int i = 0; i < 8; i++) {
                if (FaistosPath[i].hasPawn(player)) {
                    return FaistosPath[i].getPawn(player);
                }
            }
        } else if (palace == Malia) {
            for (int i = 0; i < 8; i++) {
                if (MaliaPath[i].hasPawn(player)) {
                    return MaliaPath[i].getPawn(player);
                }
            }
        } else if (palace == Zakros) {
            for (int i = 0; i < 8; i++) {
                if (ZakrosPath[i].hasPawn(player)) {
                    return ZakrosPath[i].getPawn(player);
                }
            }
        }
        return null;
    }

    /**
     * <b>Transformer</b>: sets an archeologist at beginning of a path
     *
     * @param player the player
     * @param cardToPlay the card that was played
     * @return the index of pawn array
     */
    public int ArcheologistOption(Player player, Card cardToPlay) {
        int i;
        for (i = 1; i < 4; i++) {
            if (player.getPawns()[i].getPosition() == -1) {
                if (cardToPlay.getPalace() == Knossos) {
                    player.getPawns()[i].setPosition(0);
                    player.getPawns()[i].setPalace(Knossos);
                    player.getPawns()[i].setOwner(player);
                    KnossosPath[0].setPawn(player, player.getPawns()[i]);
                    break;
                } else if (cardToPlay.getPalace() == Faistos) {
                    player.getPawns()[i].setPosition(0);
                    player.getPawns()[i].setPalace(Faistos);
                    player.getPawns()[i].setOwner(player);
                    FaistosPath[0].setPawn(player, player.getPawns()[i]);
                    break;
                } else if (cardToPlay.getPalace() == Malia) {
                    player.getPawns()[i].setPosition(0);
                    player.getPawns()[i].setPalace(Malia);
                    player.getPawns()[i].setOwner(player);
                    MaliaPath[0].setPawn(player, player.getPawns()[i]);
                    break;
                } else if (cardToPlay.getPalace() == Zakros) {
                    player.getPawns()[i].setPosition(0);
                    player.getPawns()[i].setPalace(Zakros);
                    player.getPawns()[i].setOwner(player);
                    ZakrosPath[0].setPawn(player, player.getPawns()[i]);
                    break;
                }
            }
        }
        return i;
    }

    /**
     * <b>Transformer</b>: sets Theseus at beginning of a path
     *
     * @param player the player
     * @param cardToPlay the card
     */
    public void TheseusOption(Player player, Card cardToPlay) {
        if (cardToPlay.getPalace() == Knossos) {
            player.getPawns()[0].setPosition(0);
            player.getPawns()[0].setPalace(Knossos);
            player.getPawns()[0].setOwner(player);
            KnossosPath[0].setPawn(player, player.getPawns()[0]);
        } else if (cardToPlay.getPalace() == Faistos) {
            player.getPawns()[0].setPosition(0);
            player.getPawns()[0].setPalace(Faistos);
            player.getPawns()[0].setOwner(player);
            FaistosPath[0].setPawn(player, player.getPawns()[0]);
        } else if (cardToPlay.getPalace() == Malia) {
            player.getPawns()[0].setPosition(0);
            player.getPawns()[0].setPalace(Malia);
            player.getPawns()[0].setOwner(player);
            MaliaPath[0].setPawn(player, player.getPawns()[0]);
        } else if (cardToPlay.getPalace() == Zakros) {
            player.getPawns()[0].setPosition(0);
            player.getPawns()[0].setPalace(Zakros);
            player.getPawns()[0].setOwner(player);
            ZakrosPath[0].setPawn(player, player.getPawns()[0]);
        }
    }

    /**
     * <b>Accessor(selector)</b>:Returns the position of this path
     *
     * @param palace the path
     * @param index the position index
     * @return the position of this path
     */
    public Position getPosition(Palace palace, int index) {
        if (palace == Knossos) {
            return KnossosPath[index];
        }
        if (palace == Faistos) {
            return FaistosPath[index];
        }
        if (palace == Malia) {
            return MaliaPath[index];
        }
        if (palace == Zakros) {
            return ZakrosPath[index];
        }
        return null;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the deck of cards <br/>
     * <b>Postcondition:</b> returns the deck of cards </p>
     *
     * @return the deck of cards
     */
    public ArrayList<Card> getGameCards() {
        return CardsDeck;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the game cards <br />
     * <b>Postcondition:</b> the cards are set </p>
     *
     * @param gameCards the deck of cards
     */
    public void setGameCards(ArrayList<Card> gameCards) {
        this.CardsDeck = gameCards;
    }

    /**
     * <b>Transformer:</b> Initializes and shuffles the 100 cards. Puts them on deck.
     * <b>Postcondition:</b> The cards have been put on deck shuffled.
     */
    public void initCards() {
        for (int i = 0; i < 10; i++) {
            NumberCard knossos = new NumberCard(Knossos);
            knossos.setValue(i + 1);
            knossos.setImage("images/cards/knossos" + (i + 1) + ".jpg");
            CardsDeck.add(knossos);
            CardsDeck.add(knossos);

            NumberCard malia = new NumberCard(Malia);
            malia.setValue(i + 1);
            malia.setImage("images/cards/malia" + (i + 1) + ".jpg");
            CardsDeck.add(malia);
            CardsDeck.add(malia);

            NumberCard phaistos = new NumberCard(Faistos);
            phaistos.setValue(i + 1);
            phaistos.setImage("images/cards/phaistos" + (i + 1) + ".jpg");
            CardsDeck.add(phaistos);
            CardsDeck.add(phaistos);

            NumberCard zakros = new NumberCard(Zakros);
            zakros.setValue(i + 1);
            zakros.setImage("images/cards/zakros" + (i + 1) + ".jpg");
            CardsDeck.add(zakros);
            CardsDeck.add(zakros);
        }

        Ariadne knossosAriadne = new Ariadne(Knossos);
        knossosAriadne.setImage("images/cards/knossosAri.jpg");
        Ariadne maliaAriadne = new Ariadne(Malia);
        maliaAriadne.setImage("images/cards/maliaAri.jpg");
        Ariadne phaistosAriadne = new Ariadne(Faistos);
        phaistosAriadne.setImage("images/cards/phaistosAri.jpg");
        Ariadne zakrosAriadne = new Ariadne(Zakros);
        zakrosAriadne.setImage("images/cards/zakrosAri.jpg");

        Minotaur knossosMino = new Minotaur(Knossos);
        knossosMino.setImage("images/cards/knossosMin.jpg");
        Minotaur maliaMino = new Minotaur(Malia);
        maliaMino.setImage("images/cards/maliaMin.jpg");
        Minotaur phaistosMino = new Minotaur(Faistos);
        phaistosMino.setImage("images/cards/phaistosMin.jpg");
        Minotaur zakrosMino = new Minotaur(Zakros);
        zakrosMino.setImage("images/cards/zakrosMin.jpg");

        for (int i = 0; i < 3; i++) {
            CardsDeck.add(knossosAriadne);
            CardsDeck.add(maliaAriadne);
            CardsDeck.add(phaistosAriadne);
            CardsDeck.add(zakrosAriadne);
            if (i == 2) {
                break;
            }
            CardsDeck.add(knossosMino);
            CardsDeck.add(maliaMino);
            CardsDeck.add(phaistosMino);
            CardsDeck.add(zakrosMino);
        }
        Collections.shuffle(CardsDeck);
    }

    /**
     * <b>Observer:</b> Checks if the deck of cards has no elements.
     * <b>Postcondition:</b> Returns true if the stack is empty.
     *
     * @return true if the stack of cards is empty
     */
    public boolean DeckIsEmpty() {
        if (CardsDeck.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Transformer(mutative)</b>: increases the number of pawns that passed 7th step
     */
    public void setCheckPoint() {
        CheckPoints++;
    }

    /**
     * <b>Accessor(selector)</b>: returns number of pawns that passed 7th step
     *
     * @return number of pawns that passed 7th step
     */
    public int getCheckPoints() {
        return CheckPoints;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the next player
     *
     * @param player it's his/her turn
     */
    public void setTurn(Player player) {
        this.turn = player;
    }

    /**
     * <b>Accessor(selector)</b>: returns the next player
     *
     * @return the next player
     */
    public Player getTurn() {
        return turn;
    }

    /**
     * <b>Accessor(selector)</b>: returns true if 4 pawns have passed 7th step or cards deck is empty
     *
     * @return true if the game is over, false otherwise
     */
    public boolean GameOver() {
        return GameOver;
    }

    /**
     * <b>Transformer(mutative)</b>: sets GameOver true
     */
    public void setGameOver() {
        this.GameOver = true;
    }

}
