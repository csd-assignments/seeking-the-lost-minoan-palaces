package view;

import java.awt.Image;
import java.util.Stack;
import javax.swing.*;
import model.card.Card;
import model.finding.Fresco;
import model.finding.RareFinding;
import model.player.Player;
import model.position.Position;

/**
 * The View represents whatever is shown on the screen during the execution.
 * All the components are initialized here and the image is updated as long as the game evolutes.
 * @version 1.0
 * @author marianna vel
 */
public class View extends JFrame {

    //Menu
    private JFrame framelabel = new JFrame("Αναζητώντας τα Χαμένα Μινωικά Ανάκτορα");
    private JMenu menu = new JMenu("Menu");
    private JMenuItem item1 = new JMenuItem("New Game");
    private JMenuItem item2 = new JMenuItem("Exit");

    //Table Components
    private JLayeredPaneExtension tablo;
    private JButton[] CardStack;
    private JLabel[] KnossosPath = new JLabel[9];
    private JLabel[] FaistosPath = new JLabel[9];
    private JLabel[] MaliaPath = new JLabel[9];
    private JLabel[] ZakrosPath = new JLabel[9];
    private JLabel infobox;
    private JLabel[] Pawn1 = new JLabel[4];
    private JLabel[] Pawn2 = new JLabel[4];

    //1st player's field Components
    private JLayeredPane player1_field;
    private JButton[] PlayerCards1 = new JButton[8];
    private JLabel[] PalaceLastChoice1 = new JLabel[4];
    private JLabel[] FindingImage1 = new JLabel[4];
    private Image Goddess1;
    private JButton FrescoButton1;
    private JLabel AvailablePawns1;
    private JLabel Score1;
    private JLabel StatueNumber1;

    //2nd player's field Components
    private JLayeredPane player2_field;
    private JButton[] PlayerCards2 = new JButton[8];
    private JLabel[] PalaceLastChoice2 = new JLabel[4];
    private JLabel[] FindingImage2 = new JLabel[4];
    private Image Goddess2;
    private JButton FrescoButton2;
    private JLabel AvailablePawns2;
    private JLabel Score2;
    private JLabel StatueNumber2;

    /**
     * <b>Constructor</b>: Creates a new Window and initializes some buttons and panels <br/>
     * <b>Postconditions</b>: Creates a new Window and initializes some buttons and panels starting a new game.
     */
    public View() {

        JPanel gamePanel = new JPanel();

    }

    /**
     * <b>Transformer(mutative)</b>:initializes some buttons and labels <br/>
     * <p>
     * <b>Postcondition:</b> initializes some buttons and labels </p>
     */
    private void initComponents() {

    }

    /**
     * <b>Transformer(mutative)</b>: makes the sharing of the cards
     * <b>Postcondition:</b> every player has 8 cards
     */
    public void moirasma() {

    }

    /**
     * <b>Transformer(mutative)</b>: when the user selects a card to play (or rejects a card), card is replaced by another from the deck
     * <b>Precondition:</b> card is a card from the player's collection, cards the deck of cards
     * <b>Postcondition:</b> a new card is set at player's collection
     *
     * @param c card to be replaced
     * @param cards deck of cards
     */
    public void updateCard(Card c, Stack cards) {

    }

    /**
     * <b>Accessor(selector):</b> Returns the player's collection of cards
     * <b>Precondition:</b> player is one of the 2 players
     * <b>Postcondition:</b> returns the player's collection of cards
     *
     * @param player the player whose cards will be returned
     * @return JButton[] the button cards
     */
    public JButton[] getPlayerCards(Player player) {
        return null;
    }

    /**
     * <b>Transformer(mutative):</b> Sets the last card played from player on the appropriate palace position
     * <b>Precondition:</b> player is one of the 2 players, card is a card from his/her collection
     * <b>Postcondition:</b> last card played is set on the appropriate palace position
     *
     * @param player player who played the card
     * @param c the last card played
     */
    public void setLastCard(Player player, Card c) {

    }

    /**
     * <b>Transformer(mutative):</b> sets the pawn on its new position, updates available pawns if necessary
     * <b>Precondition:</b> player is one of the 2 players, position is the new position of pawn
     * <b>Postcondition:</b> the pawn is set on its new position
     *
     * @param player one of the 2 players
     * @param position the new position of pawn
     */
    public void updatePawn(Player player, Position position) {

    }

    /**
     * <b>Transformer(mutative):</b> sets the image of the rare finding that player discovered
     * <b>Precondition:</b> player is the owner of the finding, rare is the current finding
     * <b>Postcondition:</b> the image of discovered rare finding is set
     *
     * @param player the owner of the finding
     * @param rare the current rare finding
     */
    public void updateRareFindings(Player player, RareFinding rare) {

    }

    /**
     * <b>Transformer(mutative):</b> sets the image of fresco at player's collection
     * <b>Precondition:</b> player is the owner of the finding, fresco is the current finding
     * <b>Postcondition:</b> the image of discovered fresco is set
     *
     * @param player the owner of the finding
     * @param fresco the finding that player discovered
     */
    public void updateFresco(Player player, Fresco fresco) {

    }
    
    /**
     * Opens the window with the current player's fresco paintings
     *
     * @param player the player
     */
    public void OpenFrescoWindow(Player player) {

    }

    /**
     * <b>Transformer(mutative):</b> Updates number of available cards, check points and players turn
     * <b>Postcondition:</b> number of available cards, check points and turn updated
     *
     * @param message the message that will be shown
     */
    public void updateInfobox(String message) {

    }

    /**
     * <b>Transformer(mutative):</b> increases the number of statues of the player
     * <b>Precondition:</b> player is the owner of the finding
     * <b>Postcondition:</b> number of statues increased
     *
     * @param player the owner of the finding
     */
    public void updateStatues(Player player) {

    }

    /**
     * <b>Transformer(mutative):</b> sets the new score of the player
     * <b>Precondition:</b> player is one of the 2 players and points his/her points
     * <b>Postcondition:</b> score updated
     *
     * @param player current player
     * @param points player's points
     */
    public void updateScore(Player player, int points) {

    }

    /**
     * Dialog Message appears when a player makes an excavation - discovers a finding
     * <b>Postcondition:</b> Information about the finding is shown
     *
     * @param message information about the finding
     */
    public void showDialogMessage(String message) {

    }

    /**
     * Winning Message appears at the end of the game
     * <b>Postcondition:</b> the winner is announced
     *
     * @param message the winning message
     */
    public void showWinningMessage(String message) {

    }
}
