
package view;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JLayeredPane;

/**
 * This class is used for the background image of the board.
 * @version 1.0
 * @author marianna vel
 */
public class JLayeredPaneExtension extends JLayeredPane{
    
     Image image;

        public JLayeredPaneExtension(Image icon) {
            image=icon;
        }
        
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, this);
        }
    
}
