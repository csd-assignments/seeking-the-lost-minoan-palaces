package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Stack;
import model.board.Board;
import model.card.Card;
import model.player.Player;
import model.position.Position;
import view.View;

/**
 * Controller is the master of the game and controls all of the operations executed
 *
 * @version 1.0
 * @author marianna vel
 */
public class Controller {

    Board board;
    View view;
    Player player1, player2;

    /**
     * <b>Constructor</b>: Constructs a new Controller and sets the game ready to start. 
     * So, is responsible for creating a new game and initializing it.
     * <b>Postcondition</b>: constructs a new Controller with new 2 players and initializes the game table, 
     * activates the listeners
     */
    public Controller() {
        player1 = new Player("Παίχτης1", 1);
        player2 = new Player("Παίχτης2", 2);
        board = new Board();
        view = new View();
        view.setVisible(true);
        setListeners();
    }

    /**
     * <b>Transformer(mutative)</b>: activates the listeners
     * <b>Postcondition</b>: listeners are activated
     */
    private void setListeners() {

    }

    /**
     * <b>Transformer(mutative)</b>: Removes the card and replaces it with a new one from the deck
     * <b>Postcondition</b>: the card is removed and replaced by another one
     *
     * @param positionOfCard
     */
    public void removeAndReplaceCard(int positionOfCard) {

    }
    
    /**
     * <b>Transformer(mutative)</b>: Checks if card in this position can be played above another
     * <b>Postcondition</b>: Checks if card in this position can be played above another
     * @param positionOfCard 
     */
    public void checkIfItCanBePlayed(int positionOfCard){
        
    }

    /**
     * <b>Observer</b>: Checks if the current position has finding, if it does the player gets it
     * <b>Postcondition</b>: the player has got the finding on this position
     *
     * @param pos the position
     * @return true if position has finding
     */
    public boolean checkFindingPosition(Position pos) {
        return false;
    }

    /**
     * <b>Transformer(mutative)</b>: Removes a card from the deck to put it on player's collection
     *
     * @param deck the deck of cards
     * @return top card
     */
    public Card GetCardfromDeck(Stack deck) {
        return null;
    }

    /**
     * <b>Transformer(mutative)</b>: Updates the players turn
     * <b>Postcondition</b>: the players turn has been updated
     */
    public void updateTurn() {

    }

    /**
     * <b>Observer</b>: Checks if the game has finished
     *
     * @return true if the game has finished
     */
    public boolean checkIfGameFinished() {
        return false;
    }
    
    /**
     * <b>Accessor(selector)</b>: Returns the winner
     * <b>Postcondition</b>: the winner is returned
     * @return th winner
     */
    public Player getWinner(){
        return null;        
    }
    
    /**
     * <b>Transformer(mutative)</b>: Sets the winner
     * <b>Postcondition</b>: the winner is set
     * @param winner 
     */
    public void setWinner(Player winner){
        
    }
}
//---------------------------------------------------------------------------------------------------------

/* a class which is used for doing some action after a card button has been pushed */
class CardListener implements MouseListener {

    /**
     * <b>Transformer(mutative)</b>: doing some action after a player's card has been clicked<br/>
     * <p>
     * <b>Postcondition:</b> doing some action after a player's card has been clicked</p>
     */
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}

/* a class which is used for doing some action after the deck button has been pushed */
class DeckListener implements ActionListener {

    /**
     * <b>Transformer(mutative)</b>: doing some action after the Deck button has been pushed<br/>
     * <p>
     * <b>Postcondition:</b> doing some action after the Deck button has been pushed</p>
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

}

/* a class which is used for doing some action after a fresco button has been pushed */
class FrescoListener implements ActionListener {

    /**
     * <b>Transformer(mutative)</b>: doing some action after the Fresco button has been pushed<br/>
     * <p>
     * <b>Postcondition:</b> doing some action after the Fresco button has been pushed</p>
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

}

/* a class which is used for doing some action after the Menu button has been pushed */
class MenuListener implements ActionListener {

    /**
     * <b>Transformer(mutative)</b>: doing some action after New Game or Exit button has been pushed<br/>
     * <p>
     * <b>Postcondition:</b> doing some action after New Game or Exit button has been pushed</p>
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
