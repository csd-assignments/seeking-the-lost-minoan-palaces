package main;

import controller.Controller;

/**
 * The main method.
 * @version 1.0
 * @author marianna vel
 */
public class Main {

    public static void main(String[] args) {
        Controller c = new Controller();
    }

}
