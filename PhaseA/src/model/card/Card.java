package model.card;

/**
 * Contains the methods needed for creating a number or special card for each one of the four palaces. 
 * Cannot create an object of this class.
 * @version 1.0
 * @author marianna vel
 */
public abstract class Card {

    private String palace;

    /**
     * <b>Transformer:</b> sets the palace of this card
     * <b>Precondition:</b>
     * <b>Postcondition:</b> palace card has been identified
     *
     * @param palace
     */
    public void setPalace(String palace) {
        this.palace = palace;
    }

    /**
     * <b>Accessor:</b> returns the palace of this card
     * <b>Postcondition:</b> palace has been returned
     *
     * @return the palace of this card
     */
    public String getPalace() {
        return palace;
    }

    /**
     * <b>Observer:</b> Checks if a card can be played above another
     * <b>Postcondition:</b> Returns true if this card matches parameter Card
     * @param c the card to be matched
     * @return true if this card matches parameter Card
     */
    public boolean matchCard(Card c) {
        return true;
    }
}
