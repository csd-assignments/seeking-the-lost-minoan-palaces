package model.board;

import java.util.Stack;
import model.player.Player;
import model.position.Position;

/**
 * Represents the board of the table game.
 * @version 1.0
 * @author marianna vel
 */
public class Board {

    private Stack CardsDeck;
    private Position[] KnossosPath;
    private Position[] FaistosPath;
    private Position[] MaliaPath;
    private Position[] ZakrosPath;
    private Player turn;
    private int CheckPoints;
    private boolean GameOver;

    /**
     * <b>Constructor</b>: Constructs a new Board.<br />
     * <b>Postcondition</b>: Creates and initializes the cards deck and the 4 paths.
     */
    public Board() {
        CardsDeck = new Stack();
        KnossosPath = new Position[9];
        FaistosPath = new Position[9];
        MaliaPath = new Position[9];
        ZakrosPath = new Position[9];
        this.CheckPoints = 0;
        this.GameOver = false;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the deck of cards <br/>
     * <b>Postcondition:</b> returns the deck of cards </p>
     *
     * @return the deck of cards
     */
    public Stack getGameCards() {
        return CardsDeck;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the game cards <br />
     * <b>Postcondition:</b> the cards are set </p>
     *
     * @param gameCards the deck of cards
     */
    public void setGameCards(Stack gameCards) {
        this.CardsDeck = gameCards;
    }

    /**
     * <b>Transformer:</b> Initializes and shuffles the 100 cards. Puts them on deck.
     * <b>Postcondition:</b> The cards have been put on deck shuffled.
     */
    public void initCards() {

    }

    /**
     * <b>Observer:</b> Checks if the deck of cards has no elements.
     * <b>Postcondition:</b> Returns true if the stack is empty.
     *
     * @return true if the stack of cards is empty
     */
    public boolean DeckIsEmpty() {
        return false;
    }

    /**
     * <b>Transformer(mutative)</b>: increases the number of pawns that passed 7th step
     */
    public void setCheckPoint() {
        CheckPoints++;
    }

    /**
     * <b>Accessor(selector)</b>: returns number of pawns that passed 7th step
     *
     * @return number of pawns that passed 7th step
     */
    public int getCheckPoint() {
        return CheckPoints;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the next player
     *
     * @param player it's his/her turn
     */
    public void setTurn(Player player) {
        this.turn = player;
    }

    /**
     * <b>Accessor(selector)</b>: returns the next player
     *
     * @return the next player
     */
    public Player getTurn() {
        return turn;
    }

    /**
     * <b>Accessor(selector)</b>: returns true if 4 pawns have passed 7th step or cards deck is empty
     *
     * @return true if the game is over, false otherwise
     */
    public boolean getGameOver() {
        return GameOver;
    }

    /**
     * <b>Transformer(mutative)</b>: sets GameOver true
     */
    public void setGameOver() {
        this.GameOver = true;
    }

}
