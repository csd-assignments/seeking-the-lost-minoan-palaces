package model.pawn;

import model.position.Position;

/**
 * Represents a pawn at the board.
 * Cannot create an object of this class.
 * @version 1.0
 * @author marianna vel
 */
public abstract class Pawn {

    private Position current_position;
    private boolean has_finished;

    public Pawn() {
        this.current_position = null;
        this.has_finished=false;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the current position of the pawn<br />
     * <b>Precondition:</b> parameter pos is a valid position (the new one)
     * <b>Postcondition:</b> the current position of the pawn is set </p>
     *
     * @param pos a valid position
     */
    public void setCurrentPosition(Position pos) {
        this.current_position = pos;
    }

    /**
     * <b>Accessor(selector)</b>:Returns the current position of the pawn <br/>
     * <b>Postcondition:</b> returns the current position of the pawn
     *
     * @return the current position of the pawn
     */
    public Position getCurrentPosition() {
        return current_position;
    }
    
    /**
     * <b>Observer</b>: Checks is a pawn has reached the end of its path <br/>
     * <b>Postcondition:</b> returns true if the pawn has reached the end of its path
     *
     * @return true if the pawn has reached the end of its path, false otherwise
     */
    public boolean finished(){
        return false;        
    }

}
