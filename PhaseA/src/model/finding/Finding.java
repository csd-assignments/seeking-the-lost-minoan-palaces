package model.finding;

/**
 * Contains the attributes that any finding has at this game.
 * Cannot create an object of this class.
 * @version 1.0
 * @author marianna vel
 */
public abstract class Finding {

    private int points;

    /**
     * <b>Constructor</b>: Constructs finding giving its points
     * @param points the points of the finding
     */
    public Finding(int points) {
        this.points = points;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the points of this finding
     * @return the points of the current finding
     */
    public int getPoints() {
        return points;
    }

    /**
     * <b>Transformer(mutative)</b>: sets to the finding its points
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }
}
