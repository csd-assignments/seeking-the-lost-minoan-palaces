package model.player;

import java.util.ArrayList;
import model.card.Card;
import model.finding.Finding;
import model.finding.Fresco;
import model.finding.RareFinding;
import model.palace.Palace;
import model.pawn.Pawn;
import model.position.Position;

/**
 * Player class describes the characteristics of a player and provides modification methods.
 *
 * @version 1.0
 * @author marianna vel
 */
public class Player implements PlayerADT {

    private String name;
    private Card[] cards; //new Card[8]
    private ArrayList<RareFinding> rares;
    private ArrayList<Fresco> frescos;
    private int goddess_num;
    private Pawn[] pawns; //new Pawn[4]
    private int ID; //1 or 2
    private int score;
    //listes apo tis kartes pou exoun paixtei
    private ArrayList<Card> PlayedKnossos;
    private ArrayList<Card> PlayedFaistos;
    private ArrayList<Card> PlayedMalia;
    private ArrayList<Card> PlayedZakros;

    /**
     * <b>Constructor</b>: Constructs a new Player with no name.<br />
     * <b>Postcondition</b>: Creates and initializes a player with no name.
     */
    public Player() {
        this("Anonymous", 0);
    }

    /**
     * <b>Constructor</b>: Constructs a new Player with the given parameter name.<br />
     * <b>Postcondition</b>: Creates and initializes a player with the given name. Also initializes their cards collection and their choice to 0. They have neither played nor finished at the beginning of the game.
     *
     * @param name is the name of the player.
     */
    public Player(String name, int ID) {

    }

    /**
     * <b>Transformer(mutative)</b>: It initializes a player for a new card sharing <br />
     * <b>Postcondition:</b> initializes a player for a new card sharing
     */
    public void init_player() {

    }

    /**
     * <b>Accessor(selector)</b>:Returns the name of the player <br/>
     * <p>
     * <b>Postcondition:</b> returns the name of the player </p>
     *
     * @return the name of the player
     */
    public String getName() {
        return name;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the name of the player to name <br />
     * <p>
     * <b>Postcondition:</b> the name of this player is set as name</p>
     *
     * @param name the name of the player
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the ID of a player. <br />
     * <b>Postcondition</b>: Returns the ID of the player (1 / 2)
     *
     * @return the ID of the player (1 / 2)
     */
    public int getID() {
        return ID;
    }

    /**
     * <b>Transformer(mutative)</b>: It sets the ID of a player <br />
     * <b>Postcondition</b>:the ID of the player is changed to id
     *
     * @param id the new ID of the player
     */
    public void setID(int id) {
        this.ID = id;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the score of a player
     * <b>Postcondition:</b> Returns the total number of points a player has gathered
     *
     * @return the points of a player
     */
    public int getScore() {
        return score;
    }

    /**
     * <b>Transformer(mutative)</b>: sets the new score of a player
     * <b>Precondition:</b> parameter score is a positive or negative integer
     *
     * @param score the total number of points a player has gathered
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * <b>Accessor(selector)</b>:Returns a card from the player's collection <br />
     * <b>Postcondition:</b> returns a card from the player's collection </p>
     *
     * @param index the position of the card at player's collection of cards
     * @return a card from the player's collection of cards
     */
    public Card getCard(int index) {
        return null;
    }

    /**
     * <b>Transformer(mutative)</b>: adds a Card to player's cards  <br />
     * <b>Postcondition:</b> a card is added to players cards</p>
     *
     * @param c the card that will be added to players collection
     */
    public void setCard(Card c) {

    }

    /**
     * <b>Transformer(mutative)</b>: Valuates the cardforplaying. 
     * - Sets the card that a player wants to play to this palace path. 
     * - Removes the card from player's collection.
     *
     * @param cardforplaying the new card that player wants to play
     */
    public void playCard(Card cardforplaying) {

    }
    
    /**
     * <b>Transformer(mutative)</b>: Rejects a card, makes it useless and replaces it
     * <b>Postcondition:</b> the card has been removed from the palyer's collection and replaced from another
     * @param card the card to be rejected
     */
    public void rejectCard(Card card){
        
    }

    /**
     * <b>Accessor(selector)</b>:Returns the last card that this player played at this palace<br />
     * <b>Postcondition:</b> returns the last card that this player played at this palace </p>
     *
     * @param palace
     * @return the last card that this player played at this palace
     */
    public Card getLastCardPlayed(Palace palace) {
        return null;
    }

    /**
     * <b>Transformer:</b> gets a card from the deck and adds it at player's collection
     * <b>Preconditions:</b> c is a card from the deck
     * <b>Postcondition:</b> new card is added at player's collection of cards
     *
     * @param c the new card of the current player
     */
    public void GetCardFromDeck(Card c) {

    }

    /**
     * <b>Transformer:</b> Depending on the card moves the pawn steps front or back
     * <b>Preconditions:</b> pawn is a valid pawn, position a valid position or null
     * <b>Postcondition:</b> the pawn is moved at new position
     *
     * @param pawn the pawn that will be moved
     * @param position the position of the pawn before moving
     */
    public void movePawn(Pawn pawn, Position position) {

    }

    /**
     * <b>Transformer(mutative)</b>: puts the finding at the appropriate ArrayList or increases the statues number
     * <b>Precondition:</b> parameter finding has to be a snakegoddess, a fresco or a rarefinding
     *
     * @param finding the finding at pawn's position
     */
    public void setFinding(Finding finding) {

    }

    /**
     * <b>Accessor:</b> returns all the rare findings
     * <b>Postcondition:</b> all the rare findings have been returned
     *
     * @return all the rare findings
     */
    public ArrayList<RareFinding> getRareFindings() {
        return rares;
    }

    /**
     * <b>Accessor:</b> returns all the frescos
     * <b>Postcondition:</b> all the frescos has been returned
     *
     * @return all the frescos
     */
    public ArrayList<Fresco> getFrescos() {
        return frescos;
    }

    /**
     * <b>Accessor(selector)</b>: Returns the total number of goddess statues from the player's collection <br />
     * <b>Postcondition:</b> returns the number of goddess statues from the player's collection </p>
     *
     * @return the number of godess statues of the current player
     */
    public int NumberOfStatues() {
        return goddess_num;
    }

}
