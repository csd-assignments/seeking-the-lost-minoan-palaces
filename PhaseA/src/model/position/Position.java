package model.position;

import model.palace.Palace;

/**
 * Represents a position at a board's path.
 * Cannot create an object of this class.
 * @version 1.0
 * @author marianna vel
 */
public abstract class Position {

    private int number;
    private Palace palace;
    private int points;

    /**
     * <b>Constructor:</b> initialises palace and points of the position
     *
     * @param palace
     * @param points
     */
    public Position(int num, Palace palace, int points) {
        this.number = num;
        this.palace = palace;
        this.points = points;
    }

    /**
     * <b>Accessor:</b> returns the number of the current position
     * <b>Postcondition:</b> the number has been returned
     *
     * @return a number from 1 to 9
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * <b>Transformer:</b> sets the number of this position
     * <b>Precondition:</b> number is an integer from 1 to 9
     * <b>Postcondition:</b> number of position is set
     *
     * @param number is an integer from 1 to 9
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * <b>Accessor:</b> returns the path of the palace where this position belongs
     * <b>Postcondition:</b> the palace has been returned
     *
     * @return the palace where this position belongs
     */
    public Palace getPalace() {
        return palace;
    }

    /**
     * <b>Transformer:</b> sets the palace where the path of this position leads
     * <b>Precondition:</b> palace is one of the: Knossos, Faistos, Malia, Zakros
     * <b>Postcondition:</b> path of position has been identified
     *
     * @param palace one of the 4 palaces
     */
    public void setPalace(Palace palace) {
        this.palace = palace;
    }

    /**
     * <b>Accessor:</b> returns the points of the current position
     * <b>Postcondition:</b> the points have been returned
     *
     * @return the points of the current position
     */
    public int getPoints() {
        return points;
    }

    /**
     * <b>Transformer:</b> sets the points of the current position
     * <b>Precondition:</b> points are a positive or negative integer
     * <b>Postcondition:</b> points of the position have been set
     *
     * @param int points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * <b>Observer:</b> checks if the current position has finding
     * <b>Postcondition:</b> Returns true if position has available finding
     *
     * @return true if it does, false otherwise
     */
    public boolean hasFinding() {
        return false;
    }
}
